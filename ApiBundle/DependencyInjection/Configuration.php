<?php

namespace App\AppLink\ApiBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('app_link_api');

        $treeBuilder->getRootNode()
            ->children()

                ->arrayNode('app')
                    ->children()
                        ->booleanNode('break')
                            ->defaultFalse()
                            ->info('stop an api process')
                        ->end()
                     ->end()
                    ->children()
                        ->booleanNode('log')
                            ->defaultFalse()
                            ->info('log data into database')
                        ->end()
                     ->end()
                    ->children()
                        ->booleanNode('entity')
                            ->defaultFalse()
                            ->info('add Creation/Deletion of entity with manager')
                        ->end()
                     ->end()
                    ->children()
                        ->booleanNode('parameter')
                            ->defaultFalse()
                            ->info('add parameter manager')
                        ->end()
                     ->end()
                    ->children()
                        ->booleanNode('explorer')
                            ->defaultFalse()
                            ->info('add folder viewer')
                        ->end()
                     ->end()
                ->end()

            ->end()
        ;

        return $treeBuilder;
    }
}

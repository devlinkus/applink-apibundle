<?php

namespace App\AppLink\ApiBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class AppLinkApiExtension extends Extension
{

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));

        $servicesLoaded = [];
        $servicesMissing = [];

        #optional services
        if (!empty($config['app']['break'])) {
            $loader->load('services_break.yaml');
            $servicesLoaded[] = 'api.app.break';
        } else {
            $servicesMissing[] = 'api.app.break';
        }
        if (!empty($config['app']['log'])) {
            $loader->load('services_log.yaml');
            $servicesLoaded[] = 'api.app.log';
        } else {
            $servicesMissing[] = 'api.app.log';
        }
        if (!empty($config['app']['entity'])) {
            $loaderManager = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config/manager'));
            $pathMng = realpath(__DIR__ . '/../Resources/config/manager');
            $mngAdd = array_diff(scandir($pathMng), ['.', '..']);
            foreach ($mngAdd as $mng) {
                if ($mng !== '.gitignore') {
                    $loaderManager->load($mng);
                }
            }
            $servicesLoaded[] = 'api.entity.manager';
        } else {
            $servicesMissing[] = 'api.entity.manager';
        }
        if (!empty($config['app']['parameter'])) {
            $loader->load('services_parameter.yaml');
            $servicesLoaded[] = 'api.app.parameter';
        } else {
            $servicesMissing[] = 'api.app.parameter';
        }
        if (!empty($config['app']['explorer'])) {
            $loader->load('services_explorer.yaml');
            $servicesLoaded[] = 'api.app.explorer';
        } else {
            $servicesMissing[] = 'api.app.explorer';
        }


        $container->setParameter('applink_api.service_loaded', $servicesLoaded);
        $container->setParameter('applink_api.service_missing', $servicesMissing);

        $loader->load('services.yaml');
        $loader->load('apiBundle_cfg.yaml');

        $path = realpath(__DIR__ . '/../Resources/config');
        $servicesAdd = array_diff(scandir($path), ['.', '..']);
        foreach ($servicesAdd as $service) {
            if (preg_match('#^api_#', $service)) {
                $loader->load($service);
            }
        }

    }
}

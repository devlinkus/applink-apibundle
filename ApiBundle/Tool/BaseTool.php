<?php

namespace App\AppLink\ApiBundle\Tool;

/**
 * Description of BaseTool
 *
 * @author linkus
 * @deprecated since version 1 this class is obsolete
 */
abstract class BaseTool
{
    protected $status;

    protected function tooInit($url = null, $type = null)
    {
        $this->status['url'] = $url;
        $this->status['type'] = $type;
        $this->status = [
            'url' => $url,
            'type' => $type,
            'pass' => true,
            'error' => [],
        ];
    }

    protected function isa($object, $classType, $location = null, $notify = true)
    {
        if (false === $location) {
            $notify = false;
        }
        if (true === is_a($object, $classType)) {
            return true;
        }
        if ($notify) {
            $this->status['pass'] = false;
            $type = gettype($object);
            if ($type == 'object') {
                $type = get_class($object);
            }
            $msg = sprintf('Expected class: "%s", found: "%s"', $classType, $type);
            $this->status['error']['msg'] = $msg;
            $this->status['error']['location'] = $location;
        }
        return false;
    }

    protected function assert($value, $op, $expect, $location = null, $notify = true)
    {
        if (false === $location) {
            $notify = false;
        }
        $ok = $this->compareVar($value, $op, $expect);
        if ($notify and ! $ok) {
            $this->status['pass'] = false;
            $msg = sprintf('Failed to assert that [%s] %s [%s]', $value, $op, $expect);
            $this->status['error']['msg'] = $msg;
            $this->status['error']['location'] = $location;
        }
        return $ok;
    }

    protected function isNotEmpty($value, $location = null, $notify = true)
    {
        return $this->assert($value, 'isNotEmpty', null, $location, $notify);
    }

    protected function isNumeric($value, $location = null, $notify = true)
    {
        return $this->assert($value, 'isNumeric', null, $location, $notify);
    }

    protected function compareVar($value, $op, $expect)
    {
        if ($op === '=' and $value == $expect) {
            return true;
        } elseif ($op === '>' and $value > $expect) {
            return true;
        } elseif ($op === '>=' and $value >= $expect) {
            return true;
        } elseif ($op === '<' and $value < $expect) {
            return true;
        } elseif ($op === '<=' and $value <= $expect) {
            return true;
        } elseif ($op === '>=' and $value >= $expect) {
            return true;
        } elseif ($op === 'isNotEmpty' and false === empty($value)) {
            return true;
        } elseif ($op === 'isNumeric' and true === is_numeric($value)) {
            return true;
        }
        return false;
    }

    protected function getStatus()
    {
        return $this->status;
    }
}

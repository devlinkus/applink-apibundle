<?php

namespace App\AppLink\ApiBundle\Tool;

/**
 * Provide helper method for DOMXML
 *
 * @author linkus
 */
class ToolDOM
{

    /**
     * Get content inside tags
     *
     * @param \DOMNode $element
     *
     * @return string
     */
    public function DOMinnerHTML(\DOMNode $element)
    {
        $innerHTML = "";
        $children = $element->childNodes;

        foreach ($children as $child) {
            $innerHTML .= $element->ownerDocument->saveHTML($child);
        }

        return preg_replace(['#\n#', '#\s{2,}#'], ['', ' '], $innerHTML);
    }

    /**
     * Check if a node has childs
     *
     * @param \DOMNode $element
     *
     * @return boolean
     */
    public function hasChild(\DOMNode $element)
    {
        if ($element->hasChildNodes()) {
            foreach ($element->childNodes as $c) {
                if ($c->nodeType == XML_ELEMENT_NODE) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Delete a node
     *
     * @param \DOMNode $node
     */
    public function deleteNode(\DOMNode $node)
    {
        $this->deleteChildren($node);
        $parent = $node->parentNode;
        $oldnode = $parent->removeChild($node);
    }

    /**
     * Delete children node
     *
     * @param \DOMNode $node
     */
    public function deleteChildren(\DOMNode $node)
    {
        while (isset($node->firstChild)) {
            $this->deleteChildren($node->firstChild);
            $node->removeChild($node->firstChild);
        }
    }

    /**
     * Generate Xpath expression contains
     * <code>['class', ' myc1 '], ['class', ' myc2 ']</code>
     *
     * @param array $array
     *
     * @return string
     */
    public function contains(array $array)
    {
        $contains = "contains(concat(' ', normalize-space(@%s), ' '), '%s')";
        $line = [];
        foreach ($array as $data) {
            $line[] = sprintf($contains, $data[0], $data[1]);
        }
        return implode(' and ', $line);
    }
}

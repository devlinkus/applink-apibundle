<?php

namespace App\AppLink\ApiBundle\Service\App;

use \Twig_Extension;

/**
 * Check data content in TestURL
 *
 * @author linkus
 * @package testUrl
 */
class TwigAssertData extends Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('displayTestData', array($this, 'getData'), ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('assertType', array($this, 'assertType'), ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('assertEquals', array($this, 'assertEquals'), ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('assertCount', array($this, 'assertCount'), ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('assertChoice', array($this, 'assertChoice'), ['is_safe' => ['html']]),
        );
    }

    /**
     * Return a formatted data
     *
     * @param array $data container of all data
     * @param string $key key of the container
     * @param string $type formatter mode
     * @return string
     *
     * @throws \Exception
     */
    public function getData($data, $key, $type = 'string')
    {
        if (!isset($data[$key])) {
            return '<i>null</i>';
        }
        if (empty($data[$key])) {
            return '<i>empty</i>';
        }
        if (is_string($data[$key])) {
            return $data[$key];
        }
        if ($type == 'sarray') {
            $var = '';
            foreach ($data[$key] as $v) {
                $var .= $v . '<br />';
            }
            return $var;
        } elseif ($type == 'marray') {
            $var = '<small>'.print_r($data[$key], true).'</small>';
            return $var;
        } elseif ($type == 'array') {
            $var = '';
            foreach ($data[$key] as $k => $v) {
                $var .= $k . ' &nbsp;&nbsp;<strong>=></strong>&nbsp;&nbsp; ' . $v . '<br />';
            }
            return $var;
        } elseif ($type == 'date') {
            return $data[$key]->format('Y-m-d H:i:s');
        } elseif ($type == 'string') {
            return $data[$key];
        } else {
            throw new \Exception('undefined type "' . $type . '"');
        }
    }

    /**
     * Check if data value is valid
     *
     * @param array $datar container of all data
     * @param string $key key of the container
     * @param array $type authorized value
     *
     * @return string
     */
    public function assertChoice($datar, $key, array $choice)
    {
        $t = ' title="assertChoice" ';
        if (!isset($datar[$key])) {
            return '<code ' . $t . ' class="text-danger">data missing</code>';
        }
        $data = $datar[$key];
        if (in_array($data, $choice)) {
            return '<code ' . $t . ' class="text-success">' . $data . '</code>';
        } else {
            return '<code ' . $t . ' class="text-danger">' . $data . '</code>: <code class="text-info">' . implode(', ', $choice) . '</code>';
        }
    }

    /**
     * Check if data type is valid
     *
     * @param array $datar container of all data
     * @param string $key key of the container
     * @param array $type authorized types
     *
     * @return string
     */
    public function assertType($datar, $key, array $type)
    {
        $t = ' title="assertType" ';
        if (!isset($datar[$key])) {
            if (in_array('void', $type)) {
                return '<code ' . $t . ' class="text-success">void</code>';
            }
            return '<code ' . $t . ' class="text-danger">data missing</code>';
        }
        $data = $datar[$key];

        $typeOk = false;
        $dtype = gettype($data);
        foreach ($type as $ti) {
            if ($ti == $dtype) {
                $typeOk = $dtype;
                break;
            }
        }
        if ($typeOk) {
            return '<code ' . $t . ' class="text-success">' . $typeOk . '</code>';
        } else {
            return '<code ' . $t . ' class="text-danger">' . implode(', ', $type) . '</code>: <code class="text-info">' . $dtype . '</code>';
        }
    }

    /**
     * Check for specified data if the value is correct
     *
     * @param string $id
     * @param array $datar container of all data
     * @param string $key key of the container
     * @param array $equals
     *
     * @return string|void
     */
    public function assertEquals($id, $datar, $key, array $equals)
    {
        $t = '<code class="text-info" style="display:inline-block;width:26px;">eq:</code> ';
        if (!isset($equals[$id])) {
            return;
        }
        if (!isset($datar[$key])) {
            $data = 'void';
        } else {
            $data = $datar[$key];
        }
        if (!is_scalar($data)) {
            return '<td class="danger">' . $t . 'not scalar</td>';
        }
        if ($data === $equals[$id]) {
            return '<td class="success">' . $t . 'check ok</td>';
        } else {
            return '<td class="danger">' . $t . ' ' . $data . ' != ' . $equals[$id] . '</td>';
        }
    }

    /**
     * Check for specified array if the count is correct
     *
     * @param string $id
     * @param array $datar container of all data
     * @param string $key key of the container
     * @param array $equals
     *
     * @return string|void
     */
    public function assertCount($id, $datar, $key, array $equals)
    {
        $t = '<code class="text-info" style="display:inline-block;width:26px;">c:</code> ';
        if (!isset($equals[$id])) {
            return;
        }
        if (!isset($datar[$key])) {
            return '<td class="danger">' . $t . 'not found</td>';
        } else {
            $data = $datar[$key];
        }
        if (!is_array($data)) {
            return '<td class="danger">' . $t . 'not scalar</td>';
        }
        if (count($data) === $equals[$id]) {
            return '<td class="success">' . $t . 'check ok</td>';
        } else {
            return '<td class="danger">' . $t . '' . count($data) . ' != ' . $equals[$id] . '</td>';
        }
    }

    public function getName()
    {
        return 'linkus_twig_assert_data';
    }
}

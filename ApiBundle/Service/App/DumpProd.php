<?php

namespace App\AppLink\ApiBundle\Service\App;

use \Twig_Extension;

/**
 * DumpProd is a service that mock the dump()
 * function, only avaible in dev environment
 *
 * @author linkus
 */
class DumpProd extends Twig_Extension
{
    /**
     * @param string $env environment
     *
     * @return void
     */
    public function __construct($env)
    {
        dump('prod');
        if ($env !== 'prod') {
            return;
        }
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('dump', array($this, 'dump')),
        );
    }

    /**
     * Dump data content
     *
     * @param mixed $data
     */
    public function dump($data)
    {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }

    public function getName()
    {
        return 'linkus_twig_dump_prod_only';
    }
}

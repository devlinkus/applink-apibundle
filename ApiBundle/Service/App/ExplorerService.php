<?php

namespace App\AppLink\ApiBundle\Service\App;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Finder\Finder;

/**
 * Description of ExplorerManager
 *
 * @author linkus
 * @package servcice_explorer
 */
class ExplorerService
{
    /**
     * @var array registred paths of directories managed
     */
    protected $paths;
    /**
     * @var Filesystem
     */
    protected $fs;

    /**
     *
     * @param array|null $paths registred paths
     */
    public function __construct($paths)
    {
        if (empty($paths)) {
            $this->paths = [];
        } else {
            $this->paths = $paths;
        }
        $this->fs = new Filesystem();
    }

    /**
     * return a list of all paths registred
     *
     * @return array
     */
    public function getPathsList()
    {
        $list = [];
        foreach ($this->paths as $k => $p) {
            $exist = $this->fs->exists($p);
            $list[$k] = [
                'path' => true === $exist ? realpath($p) : $p,
                'exist' => $exist
            ];
        }

        return $list;
    }

    /**
     * Return list of files ordered
     *
     * @param string $key
     * @param array $params
     *
     * @return array
     *
     * @throws \Exception
     */
    public function scan($key, array $params)
    {
        if (!isset($this->paths[$key])) {
            throw new \Exception('key "' . $key . '" not found');
        }
        $path = $this->paths[$key];
        $data = [
            'exist' => $this->fs->exists($path)
        ];


        if (false === $data['exist']) {
            return $data;
        }
        $data['path'] = $path;
        $data['pathex'] = explode('/', $path);

        $finder = new Finder();

        if (!empty($params['sort'])) {
            $method = $params['sort'];
        }
        $i = 1;
        $list = [];
        foreach ($finder->files()->in($path) as $file) {
            $name = $file->getRelativePathname();
            $size = filesize($file->getRealPath());
            $date = filemtime($file->getRealPath());
            $sort = $date . $i;
            if (isset($method)) {
                $sort = $$method . $i;
            } else {
                $sort = $i;
            }
            $list[$sort] = [
                'name' => $name,
                'size' => $size,
                'date' => $date
            ];
            $i++;
        }

        $params['direction'] == 'desc' ? krsort($list) : ksort($list);
        $data['list'] = $list;

        return $data;
    }

    /**
     * Get info on a file
     *
     * @param string $key
     * @param string $name
     *
     * @return array
     *
     * @throws \Exception
     */
    public function getInfo($key, $name)
    {
        $data = [];
        if (!isset($this->paths[$key])) {
            throw new \Exception('key "' . $key . '" not found');
        }
        $data['key'] = $key;
        $data['name'] = $name;
        $data['path'] = realpath($this->paths[$key]);

        $data['exist'] = $this->fs->exists($data['path'] . '/' . $name);

        if ($data['exist']) {
            $data['file'] = realpath($data['path'] . '/' . $name);
            $data['stat'] = stat($data['file']);
            $data['md5'] = md5_file($data['file']);
        }

        return $data;
    }
}

<?php

namespace App\AppLink\ApiBundle\Service\App;

use Doctrine\ORM\EntityRepository;

/**
 * LogRepository
 *
 * @author linkus
 * @package servcice_log
 */
class ApiLogRepository extends EntityRepository
{

    /**
     * Find entities by criteria.
     * Generate a pagination system.
     *
     * @param array $params
     * @param array $order
     * @param int $limit
     * @param int $page
     *
     * @return array
     */
    public function findWithParams(array $paramsRaw = null, array $order = null, $limit = 1, $page = 1)
    {
        $offset = 0;
        $qb = $this->createQueryBuilder('l');

        $params = $this->getParams($paramsRaw);

        if (!empty($params['method'])) {
            $qb->andWhere($qb->expr()->eq('l.setMethod', ':method'));
            $qb->setParameter('method', $params['method']);
        }
        if (!empty($params['title'])) {
            $qb->andWhere($qb->expr()->eq('l.title', ':title'));
            $qb->setParameter('title', $params['title']);
        }
        if (!empty($params['type'])) {
            $qb->andWhere($qb->expr()->eq('l.type', ':type'));
            $qb->setParameter('type', $params['type']);
        }
        $qc = clone $qb;

        if (empty($order)) {
            $order = ['id' => 'DESC'];
        }
        $qb->orderBy('l.' . key($order), $order[key($order)]);
        //limit/pagination
        $qb->setMaxResults($limit);
        if ($page > 0 and $limit > 0) {
            $offset = $page * $limit - $limit;
            $qb->setFirstResult($offset);
        }
        $entities = $qb->getQuery()->getResult();
        $count = $qc->select('COUNT(l.id)')->getQuery()->getSingleScalarResult();

        return [
            'e' => $entities,
            'dql' => $qb->getQuery()->getSQL(),
            'c' => $count,
            'p' => $page - 1 >= 1 ? $page - 1 : null,
            'n' => $page * $limit < $count ? $page + 1 : null,
            'params' => $params
        ];
    }

    /**
     * Format parameters
     *
     * @param array $params
     *
     * @return array
     */
    protected function getParams($params)
    {
        $data = [];
        $paramsType = ['method', 'title', 'type'];
        foreach ($paramsType as $pt) {
            if (!isset($params[$pt])) {
                $data[$pt] = null;
                continue;
            }
            if (empty($params[$pt])) {
                $data[$pt] = null;
            } else {
                $data[$pt] = $params[$pt];
            }
        }
        return $data;
    }
}

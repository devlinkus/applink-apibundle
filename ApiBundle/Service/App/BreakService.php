<?php

namespace App\AppLink\ApiBundle\Service\App;

/**
 * Provide a way to break an (infinite) loop in script
 *
 * If activated, on each loop, a file is scanned. If a flag is present,
 * it break the loop.
 *
 * @author linkus
 * @package servcice_break
 */
class BreakService
{

    /**
     * Path to the file to put a flag
     *
     * @var string
     */
    protected $file;

    /**
     *
     * @param type $path path to the flag file
     *
     * @throws \Exception
     */
    public function __construct($path)
    {
        if (true !== is_writable($path)) {
            throw new \Exception('The directory "' . $path . '" is not writeable');
        }
        $file = $path . '/break';
        if (true !== file_exists($file)) {
            touch($file);
            chmod($file, 0777);
        }
        $this->file = $file;
    }

    /**
     * create an empty file;
     *
     * @return void
     */
    public function init()
    {
        file_put_contents($this->file, null);
    }

    /**
     * set break file as breakable
     *
     * @return void
     */
    public function setBreak()
    {
        file_put_contents($this->file, 'break');
    }

    /**
     * check if break file as flag for breaking
     *
     * @return boolean
     */
    public function isBreakable()
    {
        $data = file_get_contents($this->file);
        if (true === empty($data)) {
            return false;
        }
        return true;
    }
}

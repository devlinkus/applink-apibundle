<?php

namespace App\AppLink\ApiBundle\Service\App;

use Doctrine\ORM\EntityManager;

/**
 * Manage log entites
 *
 * @author linkus
 * @package servcice_log
 */
class ApiLogManager
{

    /**
     * @var EntityManager entity manager
     */
    protected $om;

    /**
     * @var string class name
     */
    protected $class_entity;

    /**
     * @var App\AppLink\ApiBundle\Service\App\ApiLogRepository entity repository
     */
    protected $repository;

    /**
     * @var string method name
     */
    protected $method;

    /**
     *
     * @param EntityManager $om
     * @param string $class_entity namespace entity
     */
    public function __construct(EntityManager $om, $class_entity)
    {
        $this->om = $om;
        $this->class_entity = $class_entity;
        $this->repository = $om->getRepository($class_entity);
    }

    /**
     * Create a new Log entity
     *
     * @return App\AppLink\ApiBundle\Entity\ApiAppLog
     */
    public function createEntity()
    {
        $class = $this->class_entity;
        $entity = new $class;
        $entity->setInsertedAt(new \DateTime);

        return $entity;
    }

    /**
     * Set a method name that will be added in all
     * logs generated
     *
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * Add an entry to the database
     *
     * @param string $type
     * @param string $title
     * @param mixed $content
     *
     * @throws \Exception
     */
    public function add($type, $title, $content)
    {
        $types = [
            'i' => 'info',
            'w' => 'warning',
            'd' => 'danger',
            's' => 'success',
        ];
        if (!isset($types[$type])) {
            throw new \Exception(sprintf('@Log: type "%s" don\'t exist', $type));
        }
        if (is_array($content)) {
            $datatype = 1;
            $content = json_encode($content);
        } else {
            $datatype = 0;
        }
        $entity = $this->createEntity();
        $entity
                ->setType($types[$type])
                ->setSetMethod($this->method)
                ->setTitle($title)
                ->setContent($content)
                ->setDataType($datatype)
        ;
        $this->om->persist($entity);
        $this->om->flush($entity);
    }

    /**
     * Find logs
     *
     * @param array $params
     * @param array $order
     * @param int $limit
     * @param int $page
     *
     * @return array
     */
    public function findWithParams(array $params = null, array $order = null, $limit = 20, $page = 1)
    {
        $entities = $this->repository->findWithParams($params, $order, $limit, $page);

        return $entities;
    }

    /**
     * Truncate table
     */
    public function truncate()
    {
        $cmd = $this->om->getClassMetadata($this->class_entity);
        dump($cmd);
        $connection = $this->om->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        if ($connection->getParams()['driver'] == 'pdo_sqlite') {
            $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());
            $connection->executeUpdate($q);
        } else {
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());
            $connection->executeUpdate($q);
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
        }
    }
}

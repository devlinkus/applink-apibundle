<?php

namespace App\AppLink\ApiBundle\Service\App;

use \Twig_Extension;
use Doctrine\ORM\EntityManager;

/**
 * Manage parameter entites
 *
 * @author linkus
 * @package servcice_parameter
 */
class ApiParameterManager extends Twig_Extension
{

    /**
     * @var EntityManager entity manager
     */
    protected $om;
    /**
     * @var string class name
     */
    protected $class_entity;
    /**
     * @var Doctrine\ORM\EntityRepository entity repository
     */
    protected $repository;

    /**
     *
     * @param EntityManager $om
     * @param string $class_entity namespace entity
     */
    public function __construct(EntityManager $om, $class_entity)
    {
        $this->om = $om;
        $this->class_entity = $class_entity;
        $this->repository = $om->getRepository($class_entity);
    }

    /**
     * Add/Update a parameter
     *
     * @param string $id
     * @param string $val
     */
    public function setParam($id, $val)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            $entity = new $this->class_entity;
            $entity->setId($id);
            $entity->setVal($val);
            $this->om->persist($entity);
        } else {
            $entity->setVal($val);
        }
        $this->om->flush();
    }

    /**
     * Get a parameter
     *
     * @param string $id
     *
     * @return string
     *
     * @throws \Exception
     */
    public function getParam($id)
    {
        $entity = $this->repository->find($id);
        if (!$entity) {
            throw new \Exception(__METHOD__ . ': key "' . $id . '" not found');
        }
        return $entity->getVal();
    }

    /**
     * Check if a param exist
     *
     * @param string $id
     *
     * @return bool
     *
     */
    public function exist($id)
    {
        $entity = $this->repository->find($id);

        return $entity ? true : false;
    }

    /**
     * Delete a parameter
     *
     * @param string $id
     */
    public function delParam($id)
    {
        $entity = $this->repository->find($id);
        if ($entity) {
            $this->om->remove($entity);
            $this->om->flush();
        }
    }

    /**
     * Get all parameters
     *
     * @return array
     */
    public function getParams()
    {
        $data = [];
        $entities = $this->repository->findAll();
        foreach ($entities as $e) {
            $data[$e->getId()] = $e->getVal();
        }
        return $data;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('apiParam', array($this, 'getApiParam')),
        );
    }

    public function getApiParam($id)
    {
        return $this->getParam($id);
    }

    public function getName()
    {
        return 'linkus_api_app_parameter';
    }
}

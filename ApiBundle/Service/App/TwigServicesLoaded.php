<?php

namespace App\AppLink\ApiBundle\Service\App;

use \Twig_Extension;

/**
 * Check if optional services are loaded
 *
 * @author linkus
 */
class TwigServicesLoaded extends Twig_Extension
{

    /**
     * @var array list of services loaded
     */
    protected $services_loaded;

    /**
     * @var array list of services not loaded
     */
    protected $services_missing;

    /**
     *
     * @param array $has_services list of services not loaded
     * @param array $services_missing list of services loaded
     */
    public function __construct(array $has_services, array $services_missing)
    {
        $this->services_loaded = $has_services;
        $this->services_missing = $services_missing;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('twigHasApiService', [$this, 'hasService']),
            new \Twig_SimpleFunction('twigGenMenu', [$this, 'ctrlToMenu'], ['needs_environment' => true, 'is_safe' => ['html']]),
        );
    }

    /**
     * Check if a serviceis loaded
     *
     * @param string $service_name
     *
     * @return bool
     */
    public function hasService($service_name)
    {
        return in_array($service_name, $this->services_loaded) ? true : false;
    }

    public function ctrlToMenu(\Twig_Environment $environment, $route)
    {
        $apiLink = [];
        $dbLink = [];
        $path = realpath(__DIR__ . '/../../Controller');
        $files = array_diff(scandir($path), ['.', '..']);
        foreach ($files as $file) {
            if (!is_file($path . '/' . $file)) {
                continue;
            }
            $name = str_replace(['Controller', '.php'], '', $file);
            if (preg_match('#Api#', $name)) {
                $dbLink[$name] = 'api_' . $name . '_index';
            } else {
                $apiLink[$name] = 'api_' . strtolower($name) . '_homepage';
            }
        }
        return $environment->render('@AppLinkApi/_App/_menu_link.html.twig', compact('apiLink', 'dbLink', 'route'));
    }

    /**
     * get list of all services loaded
     *
     * @return array
     */
    public function getList()
    {
        return $this->services_loaded;
    }

    public function getAllservices()
    {
        $services = [];
        foreach ($this->services_loaded as $sl) {
            $services[$sl] = true;
        }
        foreach ($this->services_missing as $sm) {
            $services[$sm] = false;
        }
        return $services;
    }

    public function getName()
    {
        return 'linkus_twig_services_loaded';
    }
}

<?php

namespace App\AppLink\ApiBundle\Service\App;

use Symfony\Component\HttpFoundation\Request;
use App\AppLink\ApiBundle\Lib\Curl\Curl;

/**
 * Description of TestURL
 *
 * @author linkus
 * @package testUrl
 */
class TestURL
{

    /**
     * @var string path to test files
     */
    protected $folderTest;

    /**
     * @var string namespace of the tool
     */
    protected $namespaceTool;

    /**
     * @var array
     */
    protected $actions;

    /**
     * @var array list of avaible methods by tools
     */
    protected $methods;

    /**
     * @var array cURL configuration
     */
    protected $cfgCurls;

    /**
     * @var array list of avaible configuration for Curl
     */
    protected $cfgCurlTitles;

    /**
     * Load configurations
     *
     * @param array $curl_configs
     * @param array $tool_configs
     */
    public function __construct(array $curl_configs, array $tool_configs)
    {
        $this->cfgCurls = $curl_configs;
        $this->cfgCurlTitles = array_keys($curl_configs);

        $this->namespaceTool = $tool_configs['namespace'];
        $this->actions = $tool_configs['action'];

        if (!empty($this->actions)) {
            foreach ($this->actions as $class => $method) {
                $this->methods[$class] = array_keys($method);
            }
        }

        $this->folderTest = realpath(__DIR__ . '/../../HTMLtest') . '/';
    }

    /**
     * Process a request and parse content
     *
     * @param Request $request
     *
     * @return array
     */
    public function processRequest(Request $request)
    {
        $static = $request->query->get('static');
        $url = trim($request->request->get('url'));
        $method = $request->request->get('method');
        $curlConfig = $request->request->get('curlCfg');

        $data = ['data' => null];

        if ($static) {
            $ar = explode('.', $static);
            $data['data'] = [
                'type' => 'static',
                'class' => $this->namespaceTool . '\\' . $ar[0],
                'className' => $ar[0],
                'method' => $ar[1],
                'id' => $ar[2],
                'entry' => $this->actions[$ar[0]][$ar[1]][$ar[2]],
            ];
            $path = $this->folderTest . $data['data']['entry']['filename'];
            $html = file_get_contents($path);
            $tool = new $data['data']['class'];
            $method = $data['data']['method'];
            $data['data']['content'] = $tool->$method($html);
        } elseif ($url and $method and $curlConfig) {
            $ar = explode('.', $method);
            $data['data'] = [
                'type' => 'live',
                'class' => $this->namespaceTool . '\\' . $ar[0],
                'className' => $ar[0],
                'method' => $ar[1],
                'id' => '#',
                'entry' => [
                    'url' => $url,
                ],
                'cfgCurlTitle' => $curlConfig,
            ];

            $tool = new $data['data']['class'];
            $method = $data['data']['method'];
            $cURL = new Curl();
            $cURL->setConfig($this->cfgCurls[$curlConfig]);
            $html = $cURL->execute($url);
            $data['data']['content'] = $tool->$method($html);
        }

        $data['actions'] = $this->actions;
        $data['methods'] = $this->methods;
        $data['cfgCurlTitles'] = $this->cfgCurlTitles;
        $data['params'] = [
            'url' => $data['data']['type'] == 'static' ? false : $url,
            'method' => $data['data']['type'] == 'static' ? false : $method,
            'curlCfg' => $data['data']['type'] == 'static' ? false : $curlConfig,
            'className' => $data['data']['type'] == 'static' ? false : $data['data']['className'],
        ];

        return $data;
    }
}

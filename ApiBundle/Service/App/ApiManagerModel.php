<?php

namespace App\AppLink\ApiBundle\Service\App;

/**
 * Model Manager
 *
 * Common method used in Manager class
 *
 * @author Linkus
 */
abstract class ApiManagerModel
{
    /**
     * @var string alias of an entity
     */
    protected $alias;

    #Tool

    /**
     * Return alias from an entity name
     *
     * @return string
     */
    public function getAlias()
    {
        if (!$this->alias) {
            $class = explode('\\', $this->class_entity);
            $this->alias = strtolower(substr(end($class), 0, 1));
        }
        return $this->alias;
    }

    /**
     * Get fields names of an entity
     *
     * @return array
     */
    public function getColumnNames()
    {
        return $this->om->getClassMetadata($this->class_entity)->getColumnNames();
    }

    public function getFieldsInfo()
    {
        $fields = $this->getColumnNames();
        $data = [];
        foreach ($fields as $field) {
            $data[$field] = $this->om->getClassMetadata($this->class_entity)->getTypeOfField($field);
        }
        return $data;
    }

    /**
     * return class name
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class_entity;
    }

    #Read Action

    /**
     * return unique result by options
     *
     * @param array $criteria
     *
     * @return object
     */
    public function findOneBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * return unique result by id
     *
     * @param integer $id
     *
     * @return object
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * return a collection of entities by options
     *
     * @param array $criteria
     * @param array $orderBy
     * @param integer $limit
     *
     * @return ArrayCollection
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null)
    {
        return $this->repository->findBy($criteria, $orderBy, $limit);
    }

    /**
     * return a QueryBuilder with options
     *
     * <pre><code>
     * criteria example:
     * [
     *  'filedname' => 0,
     *  'fieldname' => 'aaa',
     *  'eq' => ['fieldname', 1],
     *  'eq_1' => ['fieldname2', 1],
     *  'isNull' => ['fieldname'],
     *  'in' =>  ['fieldname' => [value1, value2, value3]],
     *  'between' => ['fieldname', value-min, value-max]
     * ]
     * </code></pre>
     *
     * @param array $criteria
     * @param array $orderBy
     * @param integer $limit
     * @param integer $offset
     *
     * @return QueryBuilder
     */
    public function findByOption(array $criteria = null, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->repository->findByOption($criteria, $orderBy, $limit, $offset, $this->getAlias());
    }

    /**
     * Custom findby made for KnnpPaginatorBundle
     * So without order, without limit
     *
     * @param array $criteria
     *
     * @return array [QueryBuilder, DQL]
     */
    public function findPaginate(array $criteria = null, array $orderBy = null)
    {
        $qb = $this->repository->findByOption($criteria, $orderBy, null, null, $this->getAlias());
        return [
            'qb' => $qb,
            'dql' => $qb->getDQL(),
        ];
    }

    /**
     * process entries by iterating
     *
     * <pre><code>
     * foreach ($iterableResult as $row) {
     *     $entity = $row[0];
     * }
     * </code></pre>
     *
     * @param array $criteria
     * @param array $orderBy
     * @param integer $limit
     * @param integer $offset
     *
     * @return query result:

     */
    public function iterateBy(array $criteria = null, array $orderBy = null, $limit = null, $offset = null)
    {
        $qb = $this->findByOption($criteria, $orderBy, $limit, $offset, $this->getAlias());

        return $qb->getQuery()->iterate();
    }

    /**
     * COUNT BY query
     *
     * @param array $criteria
     *
     * @return integer
     */
    public function countBy(array $criteria = null)
    {
        $qb = $this->findByOption($criteria, null, null, null, $this->getAlias());
        $qb->select('COUNT(' . $this->getAlias() . ')');

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * UPDATE BY query
     *
     * <pre><code>
     * set example:
     * [
     *      field1 => value,
     *      field2 => value
     * ]
     * </code></pre>
     *
     * @param array $set
     * @param array $criteria
     * @param integer $limit
     *
     * @return void
     */
    public function updateBy(array $set, array $criteria = null, $limit = null)
    {
        $qb = $this->findByOption($criteria, null, $limit, null, $this->getAlias());
        $this->repository->updateSetBy($qb, $set, $this->getAlias());
    }

    /**
     * DELETE query
     *
     * @param array $criteria
     * @param integer $limit
     *
     * @return void
     */
    public function deleteBy(array $criteria = null, $limit = null)
    {
        $qb = $this->findByOption($criteria, null, $limit, null, $this->getAlias());
        $qb->delete();
        $qb->getQuery()->execute();
    }

    #Persist/Flush

    /**
     * Persist an entity
     *
     * @param Entity $entity
     * @return Manager
     */
    public function persist($entity)
    {
        $this->om->persist($entity);
        return $this;
    }

    /**
     * Deatach an entity
     *
     * @param Entity $entity
     * @return Manager
     */
    public function detach($entity)
    {
        $this->om->detach($entity);
        return $this;
    }

    /**
     * Synchronizes objects with database
     *
     * @param mixed $entity
     * @return Manager
     */
    public function flush($entity = null)
    {
        $this->om->flush($entity);
        return $this;
    }

    /**
     * empty entitymanager
     * @param type $entity
     */
    public function clear()
    {
        $this->om->clear();
    }

    #Delete

    /**
     * Remove an entity
     *
     * @param mixed $entity entity or id entity
     * @param boolean $andFlush
     *
     * @return boolean
     */
    public function deleteEntity($entity, $andFlush = true)
    {
        if (is_numeric($entity)) {
            $entity = $this->find($entity);
            if (!$entity) {
                return false;
            }
        }

        $this->om->remove($entity);
        if ($andFlush) {
            $this->om->flush();
        }
        return true;
    }

    #Detach

    /**
     * Detaches an entity from the EntityManager
     *
     * @param object $entity
     */
    public function detachEntity(object $entity)
    {
        $this->om->detach($entity);
    }
}

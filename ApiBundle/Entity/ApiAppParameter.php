<?php

namespace App\AppLink\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity ApiAppParameter
 *
 * @author linkus
 * @package servcice_parameter
 *
 * @ORM\Entity()
 * @ORM\Table(name="ApiAppParameter")
 */
class ApiAppParameter
{

    /** @ORM\Id @ORM\Column(type="string", length=100)) @ORM\GeneratedValue(strategy="NONE") */
    protected $id;

    /** @ORM\Column(type="string", length=255) */
    protected $val;

    /**
     * @param string $id
     *
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $val
     *
     * @return void
     */
    public function setVal($val)
    {
        $this->val = $val;
    }

    /**
     * @return string
     */
    public function getVal()
    {
        return $this->val;
    }
}

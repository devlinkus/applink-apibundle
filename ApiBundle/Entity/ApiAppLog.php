<?php

namespace App\AppLink\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity ApiAppLog
 *
 * @author linkus
 * @package servcice_log
 *
 * @ORM\Entity(repositoryClass="App\AppLink\ApiBundle\Service\App\ApiLogRepository")
 * @ORM\Table(name="ApiAppLog")
 */
class ApiAppLog
{

    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue(strategy="AUTO") */
    protected $id;

    /** @ORM\Column(type="string", length=10) */
    protected $type;

    /** @ORM\Column(type="string", length=100) */
    protected $setMethod;

    /** @ORM\Column(type="string", length=100) */
    protected $title;

    /** @ORM\Column(type="text") */
    protected $content;

    /** @ORM\Column(type="integer") */
    protected $dataType;

    /** @ORM\Column(type="datetime") */
    protected $insertedAt;






    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $type
     *
     * @return ApiAppLog
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $setMethod
     *
     * @return ApiAppLog
     */
    public function setSetMethod($setMethod)
    {
        $this->setMethod = $setMethod;

        return $this;
    }

    /**
     * @return string
     */
    public function getSetMethod()
    {
        return $this->setMethod;
    }

    /**
     * @param string $title
     *
     * @return ApiAppLog
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $content
     *
     * @return ApiAppLog
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param integer $dataType
     *
     * @return ApiAppLog
     */
    public function setDataType($dataType)
    {
        $this->dataType = $dataType;

        return $this;
    }

    /**
     * @return integer
     */
    public function getDataType()
    {
        return $this->dataType;
    }

    /**
     * @param \DateTime $insertedAt
     *
     * @return ApiAppLog
     */
    public function setInsertedAt($insertedAt)
    {
        $this->insertedAt = $insertedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getInsertedAt()
    {
        return $this->insertedAt;
    }

    /**
     * @return string
     */
    public function getPlog()
    {
        if ($this->getDataType() == 1) {
            echo  '<pre>';
            var_export(json_decode($this->getContent(), true));
            echo '</pre>';
        } else {
            return $this->content;
        }
    }
}

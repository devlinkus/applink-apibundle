<?php

namespace App\AppLink\ApiBundle\Controller\App;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * Display Tmp Folder
 *
 * @author linkus
 * @package servcice_explorer
 */
class ExplorerController extends AbstractController
{

    /**
     * Index of explorer
     *
     * @Route("/_app/explorer", name="applink_api_explorer", methods="GET")
     *
     * @return Response
     */
    public function indexAction()
    {
        $exp = $this->get('api.app.explorer');
        $list = $exp->getPathsList();
        if (count($list) == 1) {
            return $this->redirectToRoute('applink_api_explorer_list', ['key' => key($list)]);
        }
        return $this->render('AppLinkApiBundle:_App:explorer_home.html.twig', [
                    'list' => $list
        ]);
    }

    /**
     * List of files in a directory
     *
     * @Route("/_app/explorer/{key}", name="applink_api_explorer_list", methods="GET", requirements={
     *  "key": "\w+"
     * })
     *
     * @param Request $request
     * @param string $key id path
     *
     * @return Response
     */
    public function listAction(Request $request, $key)
    {
        $exp = $this->get('api.app.explorer');
        $params = $request->query->all();
        if (!isset($params['sort'])) {
            $params['sort'] = null;
        }
        if (!isset($params['direction'])) {
            $params['direction'] = null;
        }

        $data = $exp->scan($key, $params);

        return $this->render('AppLinkApiBundle:_App:explorer_list.html.twig', [
                    'key' => $key,
                    'data' => $data,
                    'params' => $params,
        ]);
    }

    /**
     * Info on a file
     *
     * @Route("/_app/explorer/show", name="applink_api_explorer_show", methods="POST", requirements={
     *  "key": "\w+"
     * })
     *
     * @param Request $request
     *
     * @return Response
     */
    public function showAction(Request $request)
    {
        $params = $request->request->all();
        $exp = $this->get('api.app.explorer');

        return $this->render('AppLinkApiBundle:_App:explorer_show.html.twig', [
                    'data' => $exp->getInfo($params['key'], $params['name']),
        ]);
    }
}

<?php

namespace App\AppLink\ApiBundle\Controller\App;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\AppLink\ApiBundle\Service\App\ApiParameterManager;

/**
 * Display custom parameters
 *
 * @author linkus
 * @package servcice_parameter
 */
class ParameterController extends AbstractController
{

    /**
     * Display all parameters
     *
     * @Route("/_app/parameter", name="applink_api_parameter", methods="GET")
     *
     * @param ApiParameterManager $apiParams
     *
     * @return Response
     */
    public function indexAction(ApiParameterManager $apiParams)
    {
        return $this->render('@AppLinkApi/_App/parameter.html.twig', [
                    'params' =>$apiParams->getParams(),
        ]);
    }

    /**
     * Delete a parameter
     *
     * @Route("/_app/parameter/del", name="applink_api_parameter_del", methods="DELETE")
     *
     * @param ApiParameterManager $apiParams
     * @param Request $request
     *
     * @return RedirectResponse
     */

    public function deleteAction(ApiParameterManager $apiParams, Request $request)
    {
        $id = $request->request->get('id');
        $apiParams->delParam($id);

        return $this->redirectToRoute('applink_api_parameter');
    }
    /**
     * Add a parameter
     *
     * @Route("/_app/parameter/add", name="applink_api_parameter_add", methods="POST")
     *
     * @param ApiParameterManager $apiParams
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function addAction(ApiParameterManager $apiParams, Request $request)
    {
        $id = $request->request->get('id');
        $val = $request->request->get('val');
        $apiParams->setParam($id, $val);

        return $this->redirectToRoute('applink_api_parameter');
    }
}

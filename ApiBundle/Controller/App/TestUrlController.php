<?php

namespace App\AppLink\ApiBundle\Controller\App;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use App\AppLink\ApiBundle\Service\App\TestURL;

/**
 * Tool for parsing data
 *
 * @author linkus
 * @package testUrl
*/
class TestUrlController extends AbstractController
{

    /**
     * Show formated parsing data
     *
     * @Route("/_app/testUrl", name="applink_api_testUrl", methods={"GET", "POST"})
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function testUrlAction(Request $request)
    {
        $curl_configs = $this->getParameter('api.curl.cfg');
        $tool_configs = $this->getParameter('api.testUrl.cfg');
        $testUrl = new TestURL($curl_configs, $tool_configs);

        $data = $testUrl->processRequest($request);

        return $this->render('@AppLinkApi\_App\testUrl.html.twig', [
                    'actions' => $data['actions'],
                    'methods' => $data['methods'],
                    'cfgCurlTitles' => $data['cfgCurlTitles'],
                    'data' => $data['data'],
                    'params' => $data['params']
        ]);
    }

    /**
     * Display avaible configuration for cURL
     *
     * @Route("/_app/testUrl_config", name="applink_api_toolConfig", methods="GET")
     *
     * @return Response
     */
    public function showTestUrlConfigAction()
    {
        return $this->render('@AppLinkApi\_App\show_tool_config.html.twig', [
                    'data' => $this->getParameter('api.testUrl.cfg')
        ]);
    }

    /**
     * Display Static file
     *
     * @Route("/_app/testUrl/file/{type}/{filename}", name="applink_api_testUrlFile", methods="GET", requirements={
     *  "type": "html|raw",
     *  "filename": ".+"
     * })
     *
     * @param string $type
     * @param string $filename
     *
     * @return Response
     */
    public function showStaticFileAction($type, $filename)
    {
        $path = realpath(__DIR__ . '/../../HTMLtest') . '/' . $filename;
        if (true === file_exists($path)) {
            $data = file_get_contents($path);
        } else {
            $data = null;
        }
        return $this->render('@AppLinkApi\_App\testUrlFile.html.twig', [
                    'type' => $type,
                    'data' => $data,
                    'filename' => $filename,

        ]);
    }
}

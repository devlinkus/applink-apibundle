<?php

namespace App\AppLink\ApiBundle\Controller\App;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\AppLink\ApiBundle\Service\App\ApiLogManager;
/**
 * Display log data
 *
 * @author linkus
 * @package servcice_log
 */
class LogController extends AbstractController
{

    /**
     * List of logs
     *
     * @Route("/_app/log/{page}", name="applink_api_log", defaults={"page"="1"}, methods="GET", requirements={
     *  "page" = "\d+"
     * })
     *
     * @param Request $request
     */
    public function indexAction(ApiLogManager $am, Request $request, $page)
    {
        $params = $request->query->all();
        $limit = $this->getParameter('api.app.log.limit.view')['static'];

        $entities = $am->findWithParams($params, [], $limit, $page);
        $entities['params']['page'] = $page;
        return $this->render('@AppLinkApi/_App/log.html.twig', [
            'entities' => $entities['e'],
            'count' => $entities['c'],
            'prev' => $entities['p'],
            'next' => $entities['n'],
            'params' => $entities['params'],
            'dql' => $entities['dql'],
        ]);
    }

    /**
     * List of logs (ajax
     *
     * @Route("/_app/log/ajax/{type}", name="applink_api_logajax", defaults={"type"="all"}, methods="GET", requirements={
     *  "type" = "all|info|success|warning|danger"
     * })
     *
     * @param Request $request
     *
     * @return Response
     */
    public function ajaxAction(ApiLogManager $am, Request $request, $type)
    {
        $params = $request->query->all();
        $params['type'] = $type == 'all' ? '' : $type;

        $entities = $am->findWithParams($params, [], $this->getParameter('api.app.log.limit.view')['ajax'], 1);

        return $this->render('@AppLinkApi/_App/log_.html.twig', [
            'entities' => $entities['e'],
            'count' => $entities['c'],
            'prev' => $entities['p'],
            'next' => $entities['n'],
            'params' => $entities['params'],
            'dql' => $entities['dql'],
        ]);
    }


    /**
     * Truncate log table
     *
     * @Route("/_app/log_truncate", name="applink_api_log_truncate", methods="POST")
     *
     * @return RedirectResponse
     */
    public function truncateAction(ApiLogManager $am)
    {
        $am->truncate();
        $this->get('session')->getFlashBag()->add('danger', 'Log cleared');
        return $this->redirectToRoute('applink_api_log');
    }
}

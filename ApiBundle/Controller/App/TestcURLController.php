<?php

namespace App\AppLink\ApiBundle\Controller\App;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\AppLink\ApiBundle\Lib\Curl\Curl;
use Symfony\Component\HttpFoundation\Response;

/**
 * Tool for testing a request
 *
 * @author linkus
 * @package testcUrl
 */
class TestcURLController extends AbstractController
{

    /**
     * Show information about a request
     *
     * @Route("/_app/testcURL", name="applink_api_testcURL", methods={"POST", "GET"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function testcURLAction(Request $request)
    {
        $url = $request->request->get('url');
        $config_set = $request->request->get('config_set');
        $method = $request->request->get('method');
        $params = $request->request->get('params');

        $curl_config = $this->getParameter('api.curl.cfg');
        $configs = array_keys($curl_config);

        if ($url and $config_set) {
            $c = new Curl;
            $c->setConfig($curl_config[$config_set]);
            if ($method == 'post' or $method == 'posthtml') {
                $html = $c->post($this->getParams($params), $url);
            } elseif ($method == 'dl') {
                $file = $this->generate_file($url);
                $c->dl($file, $url);
                $html = null;
            } else {
                $html = $c->execute($url);
            }

            $getDefaultOpt = $c->getDefaultOpt();
            $getHeaderRequest = $c->getHeaderRequest();
            $getHeaderResponse = $c->getHeaderResponse();
            $getHttpCode = $c->getHttpCode();
            $getTransferInfo = $c->getTransferInfo();
            $getCookie = $c->getCookie();
            $getCurlError = $c->getCurlError();
        } else {
            $getDefaultOpt = null;
            $getHeaderRequest = null;
            $getHeaderResponse = null;
            $getHttpCode = null;
            $getTransferInfo = null;
            $getCookie = null;
            $getCurlError = null;
            $html = '<em>Empty Url</em>';
        }

        if ($method == 'gethtml' or $method == 'posthtml') {
            $showHtml = true;
        } elseif ($method == 'dl') {
            $showHtml = 'dl';
        } else {
            $showHtml = null;
        }

        return $this->render('@AppLinkApi/_App/testcURL.htm.twig', [
                    'url' => $url,
                    'params' => $params,
                    'config_set' => $config_set,
                    'configs' => $configs,
                    'getDefaultOpt' => $getDefaultOpt,
                    'html' => $html,
                    'getHeaderRequest' => $getHeaderRequest,
                    'getHeaderResponse' => $getHeaderResponse,
                    'getHttpCode' => $getHttpCode,
                    'getTransferInfo' => $getTransferInfo,
                    'getCookie' => $getCookie,
                    'getCurlError' => $getCurlError,
                    'showHtml' => $showHtml,
        ]);
    }

    /**
     * Display avaible configuration for cURL
     *
     * @Route("/_app/testcURL_config", name="applink_api_curlConfig", methods="GET")
     *
     * @return Response
     */
    public function showTestcURLConfigAction()
    {
        return $this->render('@AppLinkApi/_App/show_curl_config.html.twig', [
                    'data' => $this->getParameter('api.curl.cfg')
        ]);
    }

    /**
     * Display list of files downloaded as test by cURL
     *
     * @Route("/_app/testcURL_files/{tpl}", name="applink_api_curlfiles", defaults={"tpl"=""}, methods="GET",
     *  requirements={"tpl"="_?"}
     * )
     *
     * @param string $tpl template prefix name
     *
     * @return Response
     */
    public function indexTestcURLFileAction($tpl)
    {
        $param = $this->getParameter('api.curltest.param');
        $path = $param['download.path'];
        if (true !== file_exists($path)) {
            $files = [];
        } else {
            $files = array_diff(scandir($path, SCANDIR_SORT_DESCENDING), ['.', '..']);
        }

        return $this->render('@AppLinkApi/_App/'.$tpl.'show_curl_files.html.twig', [
                    'files' => $files
        ]);
    }

    /**
     * Display file downloaded as test by cURL
     *
     * @Route("/_app/testcURL_file/{file}", name="applink_api_curlshowfile", methods="GET")
     *
     * @param string $file file name
     *
     * @return Response
     */
    public function showTestcURLFileAction($file)
    {
        $param = $this->getParameter('api.curltest.param');
        $path = $param['download.path'].'/'.$file;
        if (true !== file_exists($path)) {
            return $this->createNotFoundException('file ' . $file . ' not found');
        }

        $data = file_get_contents($path);
        $rsp = new Response();
        $rsp->headers->set('Content-Type', mime_content_type($path));
        $rsp->setContent($data);

        return $rsp;
    }

    /**
     * Convert string params to array
     *
     * @param string $params
     *
     * @return array
     */
    protected function getParams($params)
    {
        $posts = [];
        $params_raw = explode(';', trim($params));
        foreach ($params_raw as $data) {
            $p = explode('=', $data);
            $posts[$p[0]] = $p[1];
        }
        return $posts;
    }

    /**
     * Generate temporary file in cache dir.
     * Delete outdated files (max 5);
     *
     * @param string $url
     * @return string
     */
    protected function generate_file($url)
    {
        $param = $this->getParameter('api.curltest.param');
        $testPath = $param['download.path'];
        if (true !== file_exists($testPath)) {
            mkdir($testPath);
        } else {
            $files = array_diff(scandir($testPath, SCANDIR_SORT_ASCENDING), ['.', '..']);
            $del = count($files);
            while ($del > $param['max.download']) {
                $files = array_diff(scandir($testPath, SCANDIR_SORT_ASCENDING), ['.', '..']);
                if (!isset($files[2])) {
                    return;
                }
                unlink($testPath . '/' . $files[2]);
                $del--;
            }
        }
        return $testPath . '/' . time() . '_' . basename($url);
    }
}

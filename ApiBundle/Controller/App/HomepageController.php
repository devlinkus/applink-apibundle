<?php

namespace App\AppLink\ApiBundle\Controller\App;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\AppLink\ApiBundle\Service\App\TwigServicesLoaded;

class HomepageController extends AbstractController
{
    /**
     * Homepage
     *
     * @Route("/", name="applink_api_homepage", methods="GET")
     */
    public function indexAction()
    {
        return $this->render('@AppLinkApi/_App/homepage.html.twig');
    }

    /**
     * Configuration display
     *
     * @Route("/_app/config", name="applink_api_config", methods="GET")
     */
    public function configAction(TwigServicesLoaded $servicesLoaded)
    {
        return $this->render('@AppLinkApi/_App/config.html.twig', [
            'php' => $this->getPHPConfig(),
            'php_ext' => $this->getPHPExtensions(),
            's2ver' => $this->getVersion(),
            'services_app' => $servicesLoaded->getAllservices(),
            'php_version' => $this->getPHPVersion(),
            'database_name' => explode('/', $this->getParameter('database_name'))[3],
        ]);
    }

    /**
     * Display some php config values
     *
     * @return array
     */
    protected function getPHPConfig()
    {
        return [
            'max_execution_time' => intval(ini_get('max_execution_time')),
            'max_input_time' => ini_get('max_input_time'),
            'memory_limit' => ini_get('memory_limit'),
            'post_max_size' => ini_get('post_max_size'),
        ];
    }

    /**
     * Display needed php extention with version
     *
     * @return array
     */
    protected function getPHPExtensions()
    {
        $check = ['curl', 'tidy', 'json', 'dom', 'mbstring'];
        $ext_ver = [];
        foreach ($check as $ext) {
            if (in_array($ext, $check)) {
                $ext_ver[$ext] = phpversion($ext);
            }
        }
        return $ext_ver;
    }

    /**
     * Display symfony component requirement
     *
     * @return array
     */
    protected function getVersion()
    {
        $composer_file = json_decode(file_get_contents(realpath($this->getParameter('root_dir') . '/composer.json')), true);

        return $composer_file;
    }

    /**
     * PHP version
     *
     * @return string
     */
    protected function getPHPVersion()
    {
        return phpversion();
    }
}

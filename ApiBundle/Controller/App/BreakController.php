<?php

namespace App\AppLink\ApiBundle\Controller\App;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\AppLink\ApiBundle\Service\App\BreakService;

/**
 * Break function:
 *
 * display button in twig:
 *
 * <code>{{ render(controller('LinkusApiBundle:_App:showBreak')) }}</code>
 *
 * before loop:
 *
 * <code>$break = $this->container->get('api.app.break');
 * $break->init();</code>
 *
 *
 * in loop:
 * <code>
 * $isBreakable = $break->isBreakable();
 * if($isBreakable){
 *     $this->get('session')->getFlashBag()->add('info', 'script aborted by user');
 *     break;
 * }</code>
 *
 * @author linkus
 * @package servcice_break
 */
class BreakController
{

    /**
     * Stop a process managed by the service
     *
     * @Route("/_app/break_show_setBreak", name="applink_api_set_break", methods="GET")
     *
     * @return Response
     */
    public function setBreakAction(BreakService $break)
    {
        $break->init();
        $break->setBreak();

        return new Response('Ok', Response::HTTP_OK);
    }
}

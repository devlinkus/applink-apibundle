<?php

namespace App\AppLink\ApiBundle\Lib\Curl;

use App\AppLink\ApiBundle\Lib\Curl\BaseCurl;

/**
 * Manage lib cURL tool
 *
 * @author linkus
 * @version 3.1
 */
class Curl extends BaseCurl
{
    /**
     * @var bool activate/desactive header response
     */
    protected $configHeaderResponse = false;

    /**
     * @var array information about transfert session
     */
    protected $info;

    /**
     * @var string content of the header response
     */
    protected $headerResponse;

    /**
     * @var array cookies from the response
     */
    protected $cookies;

    /**
     * @var string error message about transfer
     */
    protected $curlError;

    /**
     * Parse the header
     *
     * @param callback $curl
     * @param string $header
     *
     * @return int
     */
    protected function handleResponseHeader($curl, $header)
    {
        $this->headerResponse .= $header;
        if (preg_match('#^Set-Cookie:\s*([^;]*)#', $header, $cookie) == 1) {
            $this->cookies[] = $cookie[1];
        }
        return strlen($header);
    }

    /**
     * Clear data before new session
     */
    protected function reset()
    {
        $this->info = null;
        $this->headerResponse = null;
        $this->cookies = null;
        $this->curlError = null;
    }

    /**
     * Set true to get response header
     *
     * @param bool $bool
     *
     * @return self
     */
    public function setHeaderResponse($bool = true)
    {
        $this->configHeaderResponse = $bool;

        return $this;
    }

    /**
     * Add cookies to the request
     *
     * @param array $cookies
     *
     * @return self
     */
    public function setCookiesArray(array $cookies)
    {
        $data = '';
        foreach ($cookies as $key => $value) {
            $data .= sprintf('%s=%s; ', $key, $value);
        }
        $this->setCookie($data);

        return $this;
    }

    /**
     * Execute a cURL session
     * Save transfer info
     *
     * @param string $url
     *
     * @return mixed content on success, false on failure
     */
    public function execute($url = null)
    {
        $this->reset();

        #base config;
        $this->defaultOpt[CURLOPT_RETURNTRANSFER] = true;
        $this->defaultOpt[CURLINFO_HEADER_OUT] = true;          //for header request in getTransferinfo()
        #response header
        if (true === $this->configHeaderResponse) {
            $this->defaultOpt[CURLOPT_HEADERFUNCTION] = [$this, "handleResponseHeader"];
        }

        if (isset($url) and is_string($url)) {
            $this->defaultOpt[CURLOPT_URL] = $url;
        }

        $ch = curl_init();
        curl_setopt_array($ch, $this->defaultOpt);

        $content = curl_exec($ch);
        if (curl_errno($ch)) {
            $this->curlError = curl_error($ch);
        }
        $this->info = curl_getinfo($ch);
        curl_close($ch);

        return $content;
    }

    /**
     * Send data via POST method
     *
     * @param array $post
     * @param string $url
     *
     * @return mixed content on success, false on failure
     */
    public function post(array $post, $url = null)
    {
        $this->reset();

        #base config for post
        $this->defaultOpt[CURLOPT_POST] = true;
        $this->defaultOpt[CURLOPT_RETURNTRANSFER] = true;
        $this->defaultOpt[CURLINFO_HEADER_OUT] = true;          //for header request in getTransferinfo()
        $this->defaultOpt[CURLOPT_FORBID_REUSE] = true;
        $this->defaultOpt[CURLOPT_POSTFIELDS] = $post;
        #response header
        if (true === $this->configHeaderResponse) {
            $this->defaultOpt[CURLOPT_HEADERFUNCTION] = [$this, "handleResponseHeader"];
        }

        if (isset($url) and is_string($url)) {
            $this->defaultOpt[CURLOPT_URL] = $url;
        }

        $ch = curl_init();
        curl_setopt_array($ch, $this->defaultOpt);

        $content = curl_exec($ch);
        if (curl_errno($ch)) {
            $this->curlError = curl_error($ch);
        }
        $this->info = curl_getinfo($ch);
        curl_close($ch);

        return $content;
    }

    /**
     * Download a file
     *
     * @param string $filePath absolute path of the file
     * @param string $url
     *
     * @throws \Exception
     */
    public function dl($filePath, $url = null)
    {
        $this->reset();

        $f = dirname($filePath);
        if (true !== file_exists($f)) {
            throw new \Exception(sprintf('folder %s don\'t exist', $f));
        }
        $fp = fopen($filePath, 'w+');
        $this->defaultOpt[CURLOPT_HEADER] = false;
        $this->defaultOpt[CURLOPT_RETURNTRANSFER] = false;
        $this->defaultOpt[CURLOPT_FILE] = $fp;


        #response header
        if (true === $this->configHeaderResponse) {
            $this->defaultOpt[CURLOPT_HEADERFUNCTION] = [$this, "handleResponseHeader"];
        }
        if (isset($url) && is_string($url)) {
            $this->defaultOpt[CURLOPT_URL] = $url;
        }

        $ch = curl_init();
        curl_setopt_array($ch, $this->defaultOpt);

        curl_exec($ch);
        if (curl_errno($ch)) {
            $this->curlError = curl_error($ch);
        }
        $this->info = curl_getinfo($ch);
        curl_close($ch);
        fclose($fp);
    }

    /**
     * Get transfert information
     *
     * @return array
     */
    public function getTransferInfo()
    {
        return $this->info;
    }

    /**
     * Get http_code of a transfert
     *
     * @return integer
     */
    public function getHttpCode()
    {
        return $this->info['http_code'];
    }

    /**
     * Get the header content
     *
     * @return string
     */
    public function getHeaderRequest()
    {
        if (!isset($this->defaultOpt[CURLINFO_HEADER_OUT])) {
            return 'You need to set CURLINFO_HEADER_OUT = 1 to show request header';
        }
        return isset($this->info['request_header']) ? $this->info['request_header'] : 'no header request';
    }

    /**
     * Get the header of a response IF
     * Curl::setHeaderResponse = true
     *
     * @return string
     */
    public function getHeaderResponse()
    {
        if ($this->configHeaderResponse !== true) {
            return 'Curl::setHeaderResponse need to be set at true';
        }
        return $this->headerResponse;
    }

    /**
     * Return error of the last transfert
     *
     * @return mixed null|string
     */
    public function getCurlError()
    {
        return $this->curlError;
    }

    /**
     * If http_code is invalid or error is detected, return msg.
     * Else return false
     *
     * @param mixed $http_codes valid http_code for this request
     *
     * @return mixed
     */
    public function hasErrorOnRequest($http_codes)
    {
        if (!is_null($this->curlError)) {
            return $this->curlError;
        }
        $hc = is_string($http_codes) ? array($http_codes) : $http_codes;
        if (!in_array($this->info['http_code'], $hc)) {
            return $this->info['http_code'] . ' != [' . implode(', ', $hc) . ']';
        }
        return false;
    }

    /**
     * Check if the last request was redirected
     *
     * @return boolean
     */
    public function isRedirect()
    {
        return $this->info['redirect_count'] > 0 ? true : false;
    }

    /**
     * Get cookies from the header IF
     * Curl::setHeaderResponse = true
     *
     * @return string
     */
    public function getCookie()
    {
        if ($this->configHeaderResponse !== true) {
            return 'Curl::setHeaderResponse need to be set at true';
        }
        return $this->cookies;
    }

    /**
     * Load a configuration
     *
     * @param array $cfg
     */
    public function setConfig(array $cfg)
    {
        $this->defaultOpt = null;

        foreach ($cfg as $method => $value) {
            if (method_exists($this, $method)) {
                $this->$method($value);
            } else {
                throw new \Exception(__METHOD__ . ': method "' . $method . '" don\'t exist');
            }
        }
    }
}

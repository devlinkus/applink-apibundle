<?php

namespace App\AppLink\ApiBundle\Lib\Curl;

/**
 * Create configuration by chained methods
 *
 * @author linkus
 */
abstract class BaseCurl
{
    protected $defaultOpt;
    protected $transConst = array(
        '58' => 'CURLOPT_AUTOREFERER',
        '19914' => 'CURLOPT_BINARYTRANSFER',
        '96' => 'CURLOPT_COOKIESESSION',
        '172' => 'CURLOPT_CERTINFO',
        '141' => 'CURLOPT_CONNECT_ONLY',
        '27' => 'CURLOPT_CRLF',
        '91' => 'CURLOPT_DNS_USE_GLOBAL_CACHE',
        '45' => 'CURLOPT_FAILONERROR',
        '69' => 'CURLOPT_FILETIME',
        '52' => 'CURLOPT_FOLLOWLOCATION',
        '75' => 'CURLOPT_FORBID_REUSE',
        '74' => 'CURLOPT_FRESH_CONNECT',
        '106' => 'CURLOPT_FTP_USE_EPRT',
        '85' => 'CURLOPT_FTP_USE_EPSV',
        '110' => 'CURLOPT_FTP_CREATE_MISSING_DIRS',
        '50' => 'CURLOPT_FTPAPPEND',
        '121' => 'CURLOPT_TCP_NODELAY',
        '48' => 'CURLOPT_FTPLISTONLY',
        '42' => 'CURLOPT_HEADER',
        '2' => 'CURLINFO_HEADER_OUT',
        '80' => 'CURLOPT_HTTPGET',
        '61' => 'CURLOPT_HTTPPROXYTUNNEL',
        '51' => 'CURLOPT_NETRC',
        '44' => 'CURLOPT_NOBODY',
        '43' => 'CURLOPT_NOPROGRESS',
        '99' => 'CURLOPT_NOSIGNAL',
        '47' => 'CURLOPT_POST',
        '54' => 'CURLOPT_PUT',
        '19913' => 'CURLOPT_RETURNTRANSFER',
        '-1' => 'CURLOPT_SAFE_UPLOAD',
        '64' => 'CURLOPT_SSL_VERIFYPEER',
        '53' => 'CURLOPT_TRANSFERTEXT',
        '105' => 'CURLOPT_UNRESTRICTED_AUTH',
        '46' => 'CURLOPT_UPLOAD',
        '41' => 'CURLOPT_VERBOSE',
        '98' => 'CURLOPT_BUFFERSIZE',
        '72' => 'CURLOPT_CLOSEPOLICY',
        '78' => 'CURLOPT_CONNECTTIMEOUT',
        '156' => 'CURLOPT_CONNECTTIMEOUT_MS',
        '92' => 'CURLOPT_DNS_CACHE_TIMEOUT',
        '129' => 'CURLOPT_FTPSSLAUTH',
        '84' => 'CURLOPT_HTTP_VERSION',
        '107' => 'CURLOPT_HTTPAUTH',
        '14' => 'CURLOPT_INFILESIZE',
        '19' => 'CURLOPT_LOW_SPEED_LIMIT',
        '20' => 'CURLOPT_LOW_SPEED_TIME',
        '71' => 'CURLOPT_MAXCONNECTS',
        '68' => 'CURLOPT_MAXREDIRS',
        '3' => 'CURLOPT_PORT',
        '161' => 'CURLOPT_POSTREDIR',
        '181' => 'CURLOPT_PROTOCOLS',
        '111' => 'CURLOPT_PROXYAUTH',
        '59' => 'CURLOPT_PROXYPORT',
        '101' => 'CURLOPT_PROXYTYPE',
        '182' => 'CURLOPT_REDIR_PROTOCOLS',
        '21' => 'CURLOPT_RESUME_FROM',
        '81' => 'CURLOPT_SSL_VERIFYHOST',
        '32' => 'CURLOPT_SSLVERSION',
        '33' => 'CURLOPT_TIMECONDITION',
        '13' => 'CURLOPT_TIMEOUT',
        '155' => 'CURLOPT_TIMEOUT_MS',
        '34' => 'CURLOPT_TIMEVALUE',
        '30146' => 'CURLOPT_MAX_RECV_SPEED_LARGE',
        '30145' => 'CURLOPT_MAX_SEND_SPEED_LARGE',
        '151' => 'CURLOPT_SSH_AUTH_TYPES',
        '113' => 'CURLOPT_IPRESOLVE',
        '10065' => 'CURLOPT_CAINFO',
        '10097' => 'CURLOPT_CAPATH',
        '10022' => 'CURLOPT_COOKIE',
        '10031' => 'CURLOPT_COOKIEFILE',
        '10082' => 'CURLOPT_COOKIEJAR',
        '10036' => 'CURLOPT_CUSTOMREQUEST',
        '10077' => 'CURLOPT_EGDSOCKET',
        '10102' => 'CURLOPT_ENCODING',
        '10017' => 'CURLOPT_FTPPORT',
        '10062' => 'CURLOPT_INTERFACE',
        '10026' => 'CURLOPT_KEYPASSWD',
        '10063' => 'CURLOPT_KRB4LEVEL',
        '10015' => 'CURLOPT_POSTFIELDS',
        '10004' => 'CURLOPT_PROXY',
        '10006' => 'CURLOPT_PROXYUSERPWD',
        '10076' => 'CURLOPT_RANDOM_FILE',
        '10007' => 'CURLOPT_RANGE',
        '10016' => 'CURLOPT_REFERER',
        '10162' => 'CURLOPT_SSH_HOST_PUBLIC_KEY_MD5',
        '10152' => 'CURLOPT_SSH_PUBLIC_KEYFILE',
        '10153' => 'CURLOPT_SSH_PRIVATE_KEYFILE',
        '10083' => 'CURLOPT_SSL_CIPHER_LIST',
        '10025' => 'CURLOPT_SSLCERT',
        '10026' => 'CURLOPT_SSLCERTPASSWD',
        '10086' => 'CURLOPT_SSLCERTTYPE',
        '10089' => 'CURLOPT_SSLENGINE',
        '90' => 'CURLOPT_SSLENGINE_DEFAULT',
        '10087' => 'CURLOPT_SSLKEY',
        '10026' => 'CURLOPT_SSLKEYPASSWD',
        '10088' => 'CURLOPT_SSLKEYTYPE',
        '10002' => 'CURLOPT_URL',
        '10018' => 'CURLOPT_USERAGENT',
        '10005' => 'CURLOPT_USERPWD',
        '10104' => 'CURLOPT_HTTP200ALIASES',
        '10023' => 'CURLOPT_HTTPHEADER',
        '10039' => 'CURLOPT_POSTQUOTE',
        '10028' => 'CURLOPT_QUOTE',
        '10001' => 'CURLOPT_FILE',
        '10009' => 'CURLOPT_INFILE',
        '10037' => 'CURLOPT_STDERR',
        '10029' => 'CURLOPT_WRITEHEADER',
        '20079' => 'CURLOPT_HEADERFUNCTION',
//        '???' => 'CURLOPT_PASSWDFUNCTION',
        '20056' => 'CURLOPT_PROGRESSFUNCTION',
        '20012' => 'CURLOPT_READFUNCTION',
        '20011' => 'CURLOPT_WRITEFUNCTION',
    );

    /* $bool is a bool for the following values of the parameter: */

    /**
     * TRUE to automatically set the Referer: field in requests where it follows a Location: redirect.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setAutoreferer($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_AUTOREFERER] = $bool;
        } else {
            throw new \Exception('CURLOPT_AUTOREFERER is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to return the raw output when CURLOPT_RETURNTRANSFER is used.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setBinarytransfer($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_BINARYTRANSFER] = $bool;
        } else {
            throw new \Exception('CURLOPT_BINARYTRANSFER is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to mark this as a new cookie "session". It will force libcurl to ignore all cookies it is about to load that are "session cookies" from the previous session. By default, libcurl always stores and loads all cookies, independent if they are session cookies or not. Session cookies are cookies without expiry date and they are meant to be alive and existing for this "session" only.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setCookieSession($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_COOKIESESSION] = $bool;
        } else {
            throw new \Exception('CURLOPT_COOKIESESSION is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to output SSL certification information to STDERR on secure transfers.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setCertinfo($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_CERTINFO] = $bool;
        } else {
            throw new \Exception('CURLOPT_CERTINFO is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE tells the library to perform all the required proxy authentication and connection setup, but no data transfer. This option is implemented for HTTP, SMTP and POP3.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setConnectOnly($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_CONNECT_ONLY] = $bool;
        } else {
            throw new \Exception('CURLOPT_CONNECT_ONLY is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to convert Unix newlines to CRLF newlines on transfers.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setCrlf($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_CRLF] = $bool;
        } else {
            throw new \Exception('CURLOPT_CRLF is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to use a global DNS cache. This option is not thread-safe and is enabled by default.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setDnsUseGlobalCache($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_DNS_USE_GLOBAL_CACHE] = $bool;
        } else {
            throw new \Exception('CURLOPT_DNS_USE_GLOBAL_CACHE is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to fail verbosely if the HTTP code returned is greater than or equal to 400. The default behavior is to return the page normally, ignoring the code.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setFailonerror($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_FAILONERROR] = $bool;
        } else {
            throw new \Exception('CURLOPT_FAILONERROR is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to attempt to retrieve the modification date of the remote document. This value can be retrieved using the CURLINFO_FILETIME option with curl_getinfo().
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setFiletime($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_FILETIME] = $bool;
        } else {
            throw new \Exception('CURLOPT_FILETIME is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to follow any "Location: " header that the server sends as part of the HTTP header (note this is recursive, PHP will follow as many "Location: " headers that it is sent, unless CURLOPT_MAXREDIRS is set).
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setFollowlocation($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_FOLLOWLOCATION] = $bool;
        } else {
            throw new \Exception('CURLOPT_FOLLOWLOCATION is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to force the connection to explicitly close when it has finished processing, and not be pooled for reuse.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setForbidReuse($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_FORBID_REUSE] = $bool;
        } else {
            throw new \Exception('CURLOPT_FORBID_REUSE is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to force the use of a new connection instead of a cached one.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setFreshConnect($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_FRESH_CONNECT] = $bool;
        } else {
            throw new \Exception('CURLOPT_FRESH_CONNECT is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to use EPRT (and LPRT) when doing active FTP downloads. Use FALSE to disable EPRT and LPRT and use PORT only.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setFtpUseEprt($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_FTP_USE_EPRT] = $bool;
        } else {
            throw new \Exception('CURLOPT_FTP_USE_EPRT is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to first try an EPSV command for FTP transfers before reverting back to PASV. Set to FALSE to disable EPSV.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setFtpUseEpsv($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_FTP_USE_EPSV] = $bool;
        } else {
            throw new \Exception('CURLOPT_FTP_USE_EPSV is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to create missing directories when an FTP operation encounters a path that currently doesn't exist.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setFtpCreateMissingDirs($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_FTP_CREATE_MISSING_DIRS] = $bool;
        } else {
            throw new \Exception('CURLOPT_FTP_CREATE_MISSING_DIRS is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to append to the remote file instead of overwriting it.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setFtpappend($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_FTPAPPEND] = $bool;
        } else {
            throw new \Exception('CURLOPT_FTPAPPEND is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to disable TCP's Nagle algorithm, which tries to minimize the number of small packets on the network.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setTcpNodelay($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_TCP_NODELAY] = $bool;
        } else {
            throw new \Exception('CURLOPT_TCP_NODELAY is not a boolean');
        }
        return $this;
    }

    /**
     * An alias of CURLOPT_TRANSFERTEXT. Use that instead.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setFtpascii($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_TRANSFERTEXT] = $bool;
        } else {
            throw new \Exception('CURLOPT_TRANSFERTEXT is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to only list the names of an FTP directory.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setFtplistonly($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_FTPLISTONLY] = $bool;
        } else {
            throw new \Exception('CURLOPT_FTPLISTONLY is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to include the header in the output.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setHeader($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_HEADER] = $bool;
        } else {
            throw new \Exception('CURLOPT_HEADER is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to track the handle's request string.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setHeaderOut($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLINFO_HEADER_OUT] = $bool;
        } else {
            throw new \Exception('CURLINFO_HEADER_OUT is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to reset the HTTP request method to GET. Since GET is the default, this is only necessary if the request method has been changed.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setHttpget($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_HTTPGET] = $bool;
        } else {
            throw new \Exception('CURLOPT_HTTPGET is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to tunnel through a given HTTP proxy.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setHttpproxytunnel($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_HTTPPROXYTUNNEL] = $bool;
        } else {
            throw new \Exception('CURLOPT_HTTPPROXYTUNNEL is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to be completely silent with regards to the cURL functions.
     * Note:
     * Removed in cURL 7.15.5 (You can use CURLOPT_RETURNTRANSFER instead)
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setMute($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_RETURNTRANSFER] = $bool;
        } else {
            throw new \Exception('CURLOPT_RETURNTRANSFER is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to scan the ~/.netrc file to find a username and password for the remote site that a connection is being established with.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setNetrc($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_NETRC] = $bool;
        } else {
            throw new \Exception('CURLOPT_NETRC is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to exclude the body from the output. Request method is then set to HEAD. Changing this to FALSE does not change it to GET.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setNobody($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_NOBODY] = $bool;
        } else {
            throw new \Exception('CURLOPT_NOBODY is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to disable the progress meter for cURL transfers.
     * Note:
     * PHP automatically sets this option to TRUE, this should only be changed for debugging purposes.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setNoprogress($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_NOPROGRESS] = $bool;
        } else {
            throw new \Exception('CURLOPT_NOPROGRESS is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to ignore any cURL function that causes a signal to be sent to the PHP process. This is turned on by default in multi-threaded SAPIs so timeout options can still be used.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setNosignal($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_NOSIGNAL] = $bool;
        } else {
            throw new \Exception('CURLOPT_NOSIGNAL is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to do a regular HTTP POST. This POST is the normal application/x-www-form-urlencoded kind, most commonly used by HTML forms.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setPost($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_POST] = $bool;
        } else {
            throw new \Exception('CURLOPT_POST is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to HTTP PUT a file. The file to PUT must be set with CURLOPT_INFILE and CURLOPT_INFILESIZE.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setPut($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_PUT] = $bool;
        } else {
            throw new \Exception('CURLOPT_PUT is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setReturntransfer($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_RETURNTRANSFER] = $bool;
        } else {
            throw new \Exception('CURLOPT_RETURNTRANSFER is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to disable support for the @ prefix for uploading files in CURLOPT_POSTFIELDS, which means that values starting with @ can be safely passed as fields. CURLFile may be used for uploads instead.
     * Note:
     * Added in PHP 5.5.0 with FALSE as the default value. PHP 5.6.0 changes the default value to TRUE.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setSafeUpload($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_SAFE_UPLOAD] = $bool;
        } else {
            throw new \Exception('CURLOPT_SAFE_UPLOAD is not a boolean');
        }
        return $this;
    }

    /**
     * FALSE to stop cURL from verifying the peer's certificate. Alternate certificates to verify against can be specified with the CURLOPT_CAINFO option or a certificate directory can be specified with the CURLOPT_CAPATH option.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setSslVerifypeer($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_SSL_VERIFYPEER] = $bool;
        } else {
            throw new \Exception('CURLOPT_SSL_VERIFYPEER is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to use ASCII mode for FTP transfers. For LDAP, it retrieves data in plain text instead of HTML. On Windows systems, it will not set STDOUT to binary mode.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setTransfertext($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_TRANSFERTEXT] = $bool;
        } else {
            throw new \Exception('CURLOPT_TRANSFERTEXT is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to keep sending the username and password when following locations (using CURLOPT_FOLLOWLOCATION), even when the hostname has changed.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setUnrestrictedAuth($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_UNRESTRICTED_AUTH] = $bool;
        } else {
            throw new \Exception('CURLOPT_UNRESTRICTED_AUTH is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to prepare for an upload.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setUpload($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_UPLOAD] = $bool;
        } else {
            throw new \Exception('CURLOPT_UPLOAD is not a boolean');
        }
        return $this;
    }

    /**
     * TRUE to output verbose information. Writes output to STDERR, or the file specified using CURLOPT_STDERR.
     *
     * @param bool $bool
     * @return self
     * @throws \Exception
     */
    public function setVerbose($bool)
    {
        if (is_bool($bool)) {
            $this->defaultOpt[CURLOPT_VERBOSE] = $bool;
        } else {
            throw new \Exception('CURLOPT_VERBOSE is not a boolean');
        }
        return $this;
    }

    /* $int is an integer for the following values of the parameter: */

    /**
     * The size of the buffer to use for each read. There is no guarantee this request will be fulfilled, however.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setBuffersize($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_BUFFERSIZE] = $int;
        } else {
            throw new \Exception('CURLOPT_BUFFERSIZE is not an integer');
        }
        return $this;
    }

    /**
     * One of the CURLCLOSEPOLICY_* values.
     * Note:
     * This option is deprecated, as it was never implemented in cURL and never had any effect.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setClosepolicy($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_CLOSEPOLICY] = $int;
        } else {
            throw new \Exception('CURLOPT_CLOSEPOLICY is not an integer');
        }
        return $this;
    }

    /**
     * The number of seconds to wait while trying to connect. Use 0 to wait indefinitely.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setConnecttimeout($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_CONNECTTIMEOUT] = $int;
        } else {
            throw new \Exception('CURLOPT_CONNECTTIMEOUT is not an integer');
        }
        return $this;
    }

    /**
     * The number of milliseconds to wait while trying to connect. Use 0 to wait indefinitely. If libcurl is built to use the standard system name resolver, that portion of the connect will still use full-second resolution for timeouts with a minimum timeout allowed of one second.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setConnecttimeoutMs($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_CONNECTTIMEOUT_MS] = $int;
        } else {
            throw new \Exception('CURLOPT_CONNECTTIMEOUT_MS is not an integer');
        }
        return $this;
    }

    /**
     * The number of seconds to keep DNS entries in memory. This option is set to 120 (2 minutes) by default.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setDnsCacheTimeout($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_DNS_CACHE_TIMEOUT] = $int;
        } else {
            throw new \Exception('CURLOPT_DNS_CACHE_TIMEOUT is not an integer');
        }
        return $this;
    }

    /**
     * The FTP authentication method (when is activated): CURLFTPAUTH_SSL (try SSL first), CURLFTPAUTH_TLS (try TLS first), or CURLFTPAUTH_DEFAULT (let cURL decide).
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setFtpsslauth($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_FTPSSLAUTH] = $int;
        } else {
            throw new \Exception('CURLOPT_FTPSSLAUTH is not an integer');
        }
        return $this;
    }

    /**
     * CURL_HTTP_VERSION_NONE (default, lets CURL decide which version to use), CURL_HTTP_VERSION_1_0 (forces HTTP/1.0), or CURL_HTTP_VERSION_1_1 (forces HTTP/1.1).
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setHttpVersion($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_HTTP_VERSION] = $int;
        } else {
            throw new \Exception('CURLOPT_HTTP_VERSION is not an integer');
        }
        return $this;
    }

    /**
     *  The HTTP authentication method(s) to use. The options are: CURLAUTH_BASIC, CURLAUTH_DIGEST, CURLAUTH_GSSNEGOTIATE, CURLAUTH_NTLM, CURLAUTH_ANY, and CURLAUTH_ANYSAFE.
     *
     * The bitwise | (or) operator can be used to combine more than one method. If this is done, cURL will poll the server to see what methods it supports and pick the best one.
     *
     * CURLAUTH_ANY is an alias for CURLAUTH_BASIC | CURLAUTH_DIGEST | CURLAUTH_GSSNEGOTIATE | CURLAUTH_NTLM.
     *
     * CURLAUTH_ANYSAFE is an alias for CURLAUTH_DIGEST | CURLAUTH_GSSNEGOTIATE | CURLAUTH_NTLM.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setHttpauth($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_HTTPAUTH] = $int;
        } else {
            throw new \Exception('CURLOPT_HTTPAUTH is not an integer');
        }
        return $this;
    }

    /**
     * The expected size, in bytes, of the file when uploading a file to a remote site. Note that using this option will not stop libcurl from sending more data, as exactly what is sent depends on CURLOPT_READFUNCTION.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setInfilesize($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_INFILESIZE] = $int;
        } else {
            throw new \Exception('CURLOPT_INFILESIZE is not an integer');
        }
        return $this;
    }

    /**
     * The transfer speed, in bytes per second, that the transfer should be below during the count of CURLOPT_LOW_SPEED_TIME seconds before PHP considers the transfer too slow and aborts.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setLowSpeedLimit($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_LOW_SPEED_LIMIT] = $int;
        } else {
            throw new \Exception('CURLOPT_LOW_SPEED_LIMIT is not an integer');
        }
        return $this;
    }

    /**
     * The number of seconds the transfer speed should be below CURLOPT_LOW_SPEED_LIMIT before PHP considers the transfer too slow and aborts.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setLowSpeedTime($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_LOW_SPEED_TIME] = $int;
        } else {
            throw new \Exception('CURLOPT_LOW_SPEED_TIME is not an integer');
        }
        return $this;
    }

    /**
     * The maximum amount of persistent connections that are allowed. When the limit is reached, CURLOPT_CLOSEPOLICY is used to determine which connection to close.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setMaxconnects($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_MAXCONNECTS] = $int;
        } else {
            throw new \Exception('CURLOPT_MAXCONNECTS is not an integer');
        }
        return $this;
    }

    /**
     * The maximum amount of HTTP redirections to follow. Use this option alongside CURLOPT_FOLLOWLOCATION.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setMaxredirs($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_MAXREDIRS] = $int;
        } else {
            throw new \Exception('CURLOPT_MAXREDIRS is not an integer');
        }
        return $this;
    }

    /**
     * An alternative port number to connect to.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setPort($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_PORT] = $int;
        } else {
            throw new \Exception('CURLOPT_PORT is not an integer');
        }
        return $this;
    }

    /**
     * A bitmask of 1 (301 Moved Permanently), 2 (302 Found) and 4 (303 See Other) if the HTTP POST method should be maintained when CURLOPT_FOLLOWLOCATION is set and a specific type of redirect occurs.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setPostredir($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_POSTREDIR] = $int;
        } else {
            throw new \Exception('CURLOPT_POSTREDIR is not an integer');
        }
        return $this;
    }

    /**
     *  Bitmask of CURLPROTO_* values. If used, this bitmask limits what protocols libcurl may use in the transfer. This allows you to have a libcurl built to support a wide range of protocols but still limit specific transfers to only be allowed to use a subset of them. By default libcurl will accept all protocols it supports. See also CURLOPT_REDIR_PROTOCOLS.
     *
     * Valid protocol options are: CURLPROTO_HTTP, CURLPROTO_HTTPS, CURLPROTO_FTP, CURLPROTO_FTPS, CURLPROTO_SCP, CURLPROTO_SFTP, CURLPROTO_TELNET, CURLPROTO_LDAP, CURLPROTO_LDAPS, CURLPROTO_DICT, CURLPROTO_FILE, CURLPROTO_TFTP, CURLPROTO_ALL
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setProtocols($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_PROTOCOLS] = $int;
        } else {
            throw new \Exception('CURLOPT_PROTOCOLS is not an integer');
        }
        return $this;
    }

    /**
     * The HTTP authentication method(s) to use for the proxy connection. Use the same bitmasks as described in CURLOPT_HTTPAUTH. For proxy authentication, only CURLAUTH_BASIC and CURLAUTH_NTLM are currently supported.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setProxyauth($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_PROXYAUTH] = $int;
        } else {
            throw new \Exception('CURLOPT_PROXYAUTH is not an integer');
        }
        return $this;
    }

    /**
     * The port number of the proxy to connect to. This port number can also be set in CURLOPT_PROXY.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setProxyport($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_PROXYPORT] = $int;
        } else {
            throw new \Exception('CURLOPT_PROXYPORT is not an integer');
        }
        return $this;
    }

    /**
     * Either CURLPROXY_HTTP (default), CURLPROXY_SOCKS4, CURLPROXY_SOCKS5, CURLPROXY_SOCKS4A or CURLPROXY_SOCKS5_HOSTNAME.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setProxytype($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_PROXYTYPE] = $int;
        } else {
            throw new \Exception('CURLOPT_PROXYTYPE is not an integer');
        }
        return $this;
    }

    /**
     * Bitmask of CURLPROTO_* values. If used, this bitmask limits what protocols libcurl may use in a transfer that it follows to in a redirect when CURLOPT_FOLLOWLOCATION is enabled. This allows you to limit specific transfers to only be allowed to use a subset of protocols in redirections. By default libcurl will allow all protocols except for FILE and SCP. This is a difference compared to pre-7.19.4 versions which unconditionally would follow to all protocols supported. See also CURLOPT_PROTOCOLS for protocol constant values.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setRedirProtocols($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_REDIR_PROTOCOLS] = $int;
        } else {
            throw new \Exception('CURLOPT_REDIR_PROTOCOLS is not an integer');
        }
        return $this;
    }

    /**
     * The offset, in bytes, to resume a transfer from.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setResumeFrom($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_RESUME_FROM] = $int;
        } else {
            throw new \Exception('CURLOPT_RESUME_FROM is not an integer');
        }
        return $this;
    }

    /**
     * 1 to check the existence of a common name in the SSL peer certificate. 2 to check the existence of a common name and also verify that it matches the hostname provided. In production environments the value of this option should be kept at 2 (default value).
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setSslVerifyhost($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_SSL_VERIFYHOST] = $int;
        } else {
            throw new \Exception('CURLOPT_SSL_VERIFYHOST is not an integer');
        }
        return $this;
    }

    /**
     * One of CURL_SSLVERSION_DEFAULT (0), CURL_SSLVERSION_TLSv1 (1), CURL_SSLVERSION_SSLv2 (2), CURL_SSLVERSION_SSLv3 (3), CURL_SSLVERSION_TLSv1_0 (4), CURL_SSLVERSION_TLSv1_1 (5) or CURL_SSLVERSION_TLSv1_2 (6).
     *
     * Note:
     * Your best bet is to not set this and let it use the default. Setting it to 2 or 3 is very dangerous given the known vulnerabilities in SSLv2 and SSLv3.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setSslversion($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_SSLVERSION] = $int;
        } else {
            throw new \Exception('CURLOPT_SSLVERSION is not an integer');
        }
        return $this;
    }

    /**
     * How CURLOPT_TIMEVALUE is treated. Use CURL_TIMECOND_IFMODSINCE to return the page only if it has been modified since the time specified in CURLOPT_TIMEVALUE. If it hasn't been modified, a "304 Not Modified" header will be returned assuming CURLOPT_HEADER is TRUE. Use CURL_TIMECOND_IFUNMODSINCE for the reverse effect. CURL_TIMECOND_IFMODSINCE is the default.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setTimecondition($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_TIMECONDITION] = $int;
        } else {
            throw new \Exception('CURLOPT_TIMECONDITION is not an integer');
        }
        return $this;
    }

    /**
     * The maximum number of seconds to allow cURL functions to execute.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setTimeout($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_TIMEOUT] = $int;
        } else {
            throw new \Exception('CURLOPT_TIMEOUT is not an integer');
        }
        return $this;
    }

    /**
     * The maximum number of milliseconds to allow cURL functions to execute. If libcurl is built to use the standard system name resolver, that portion of the connect will still use full-second resolution for timeouts with a minimum timeout allowed of one second.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setTimeoutMs($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_TIMEOUT_MS] = $int;
        } else {
            throw new \Exception('CURLOPT_TIMEOUT_MS is not an integer');
        }
        return $this;
    }

    /**
     * The time in seconds since January 1st, 1970. The time will be used by CURLOPT_TIMECONDITION. By default, CURL_TIMECOND_IFMODSINCE is used.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setTimevalue($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_TIMEVALUE] = $int;
        } else {
            throw new \Exception('CURLOPT_TIMEVALUE is not an integer');
        }
        return $this;
    }

    /**
     * If a download exceeds this speed (counted in bytes per second) on cumulative average during the transfer, the transfer will pause to keep the average rate less than or equal to the parameter value. Defaults to unlimited speed.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setMaxRecvSpeedLarge($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_MAX_RECV_SPEED_LARGE] = $int;
        } else {
            throw new \Exception('CURLOPT_MAX_RECV_SPEED_LARGE is not an integer');
        }
        return $this;
    }

    /**
     * If an upload exceeds this speed (counted in bytes per second) on cumulative average during the transfer, the transfer will pause to keep the average rate less than or equal to the parameter value. Defaults to unlimited speed.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setMaxSendSpeedLarge($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_MAX_SEND_SPEED_LARGE] = $int;
        } else {
            throw new \Exception('CURLOPT_MAX_SEND_SPEED_LARGE is not an integer');
        }
        return $this;
    }

    /**
     * A bitmask consisting of one or more of CURLSSH_AUTH_PUBLICKEY, CURLSSH_AUTH_PASSWORD, CURLSSH_AUTH_HOST, CURLSSH_AUTH_KEYBOARD. Set to CURLSSH_AUTH_ANY to let libcurl pick one.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setSshAuthTypes($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_SSH_AUTH_TYPES] = $int;
        } else {
            throw new \Exception('CURLOPT_SSH_AUTH_TYPES is not an integer');
        }
        return $this;
    }

    /**
     * Allows an application to select what kind of IP addresses to use when resolving host names. This is only interesting when using host names that resolve addresses using more than one version of IP, possible values are CURL_IPRESOLVE_WHATEVER, CURL_IPRESOLVE_V4, CURL_IPRESOLVE_V6, by default CURL_IPRESOLVE_WHATEVER.
     *
     * @param int $int
     * @return self
     * @throws \Exception
     */
    public function setIpresolve($int)
    {
        if (is_int($int)) {
            $this->defaultOpt[CURLOPT_IPRESOLVE] = $int;
        } else {
            throw new \Exception('CURLOPT_IPRESOLVE is not an integer');
        }
        return $this;
    }

    /* $string is a string for the following values of the parameter: */

    /**
     * The name of a file holding one or more certificates to verify the peer with. This only makes sense when used in combination with CURLOPT_SSL_VERIFYPEER.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setCainfo($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_CAINFO] = $string;
        } else {
            throw new \Exception('CURLOPT_CAINFO is not a string');
        }
        return $this;
    }
    /**
     * (Custom method)
     * The name of a file holding one or more certificates to verify the peer with. This only makes sense when used in combination with CURLOPT_SSL_VERIFYPEER.
     * Similar to setCainfo but path related to "crt/"
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setRelCainfo($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_CAINFO] = realpath(__DIR__).'/crt/'.$string;
        } else {
            throw new \Exception('CURLOPT_CAINFO is not a string');
        }
        return $this;
    }

    /**
     * A directory that holds multiple CA certificates. Use this option alongside CURLOPT_SSL_VERIFYPEER.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setCapath($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_CAPATH] = $string;
        } else {
            throw new \Exception('CURLOPT_CAPATH is not a string');
        }
        return $this;
    }

    /**
     * The contents of the "Cookie: " header to be used in the HTTP request. Note that multiple cookies are separated with a semicolon followed by a space (e.g., "fruit=apple; colour=red")
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setCookie($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_COOKIE] = $string;
        } else {
            throw new \Exception('CURLOPT_COOKIE is not a string');
        }
        return $this;
    }

    /**
     * The name of the file containing the cookie data. The cookie file can be in Netscape format, or just plain HTTP-style headers dumped into a file. If the name is an empty string, no cookies are loaded, but cookie handling is still enabled.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setCookiefile($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_COOKIEFILE] = $string;
        } else {
            throw new \Exception('CURLOPT_COOKIEFILE is not a string');
        }
        return $this;
    }

    /**
     * The name of a file to save all internal cookies to when the handle is closed, e.g. after a call to curl_close.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setCookiejar($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_COOKIEJAR] = $string;
        } else {
            throw new \Exception('CURLOPT_COOKIEJAR is not a string');
        }
        return $this;
    }

    /**
     * A custom request method to use instead of "GET" or "HEAD" when doing a HTTP request. This is useful for doing "DELETE" or other, more obscure HTTP requests. Valid values are things like "GET", "POST", "CONNECT" and so on; i.e. Do not enter a whole HTTP request line here. For instance, entering "GET /index.html HTTP/1.0\r\n\r\n" would be incorrect.
     * Note:
     * Don't do this without making sure the server supports the custom request method first.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setCustomrequest($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_CUSTOMREQUEST] = $string;
        } else {
            throw new \Exception('CURLOPT_CUSTOMREQUEST is not a string');
        }
        return $this;
    }

    /**
     * Like CURLOPT_RANDOM_FILE, except a filename to an Entropy Gathering Daemon socket.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setEgdsocket($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_EGDSOCKET] = $string;
        } else {
            throw new \Exception('CURLOPT_EGDSOCKET is not a string');
        }
        return $this;
    }

    /**
     * The contents of the "Accept-Encoding: " header. This enables decoding of the response. Supported encodings are "identity", "deflate", and "gzip". If an empty string, "", is set, a header containing all supported encoding types is sent.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setEncoding($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_ENCODING] = $string;
        } else {
            throw new \Exception('CURLOPT_ENCODING is not a string');
        }
        return $this;
    }

    /**
     * The value which will be used to get the IP address to use for the FTP "PORT" instruction. The "PORT" instruction tells the remote server to connect to our specified IP address. The string may be a plain IP address, a hostname, a network interface name (under Unix), or just a plain '-' to use the systems default IP address.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setFtpport($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_FTPPORT] = $string;
        } else {
            throw new \Exception('CURLOPT_FTPPORT is not a string');
        }
        return $this;
    }

    /**
     * The name of the outgoing network interface to use. This can be an interface name, an IP address or a host name.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setInterface($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_INTERFACE] = $string;
        } else {
            throw new \Exception('CURLOPT_INTERFACE is not a string');
        }
        return $this;
    }

    /**
     * The password required to use the CURLOPT_SSLKEY or CURLOPT_SSH_PRIVATE_KEYFILE private key.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setKeypasswd($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_KEYPASSWD] = $string;
        } else {
            throw new \Exception('CURLOPT_KEYPASSWD is not a string');
        }
        return $this;
    }

    /**
     * The KRB4 (Kerberos 4) security level. Any of the following values (in order from least to most powerful) are valid: "clear", "safe", "confidential", "private".. If the string does not match one of these, "private" is used. Setting this option to NULL will disable KRB4 security. Currently KRB4 security only works with FTP transactions.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setKrb4level($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_KRB4LEVEL] = $string;
        } else {
            throw new \Exception('CURLOPT_KRB4LEVEL is not a string');
        }
        return $this;
    }

    /**
     * The full data to post in a HTTP "POST" operation. To post a file, prepend a filename with @ and use the full path. The filetype can be explicitly specified by following the filename with the type in the format ';type=mimetype'. This parameter can either be passed as a urlencoded string like 'para1=val1&para2=val2&...' or as an array with the field name as key and field data as value. If value is an array, the Content-Type header will be set to multipart/form-data. As of PHP 5.2.0, value must be an array if files are passed to this option with the @ prefix. As of PHP 5.5.0, the @ prefix is deprecated and files can be sent using CURLFile. The @ prefix can be disabled for safe passing of values beginning with @ by setting the CURLOPT_SAFE_UPLOAD option to TRUE.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setPostfields($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_POSTFIELDS] = $string;
        } else {
            throw new \Exception('CURLOPT_POSTFIELDS is not a string');
        }
        return $this;
    }

    /**
     * The HTTP proxy to tunnel requests through.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setProxy($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_PROXY] = $string;
        } else {
            throw new \Exception('CURLOPT_PROXY is not a string');
        }
        return $this;
    }

    /**
     * A username and password formatted as "[username]:[password]" to use for the connection to the proxy.
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setProxyuserpwd($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_PROXYUSERPWD] = $string;
        } else {
            throw new \Exception('CURLOPT_PROXYUSERPWD is not a string');
        }
        return $this;
    }

    /**
     * A filename to be used to seed the random number generator for SSL.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setRandomFile($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_RANDOM_FILE] = $string;
        } else {
            throw new \Exception('CURLOPT_RANDOM_FILE is not a string');
        }
        return $this;
    }

    /**
     * Range(s) of data to retrieve in the format "X-Y" where X or Y are optional. HTTP transfers also support several intervals, separated with commas in the format "X-Y,N-M".
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setRange($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_RANGE] = $string;
        } else {
            throw new \Exception('CURLOPT_RANGE is not a string');
        }
        return $this;
    }

    /**
     * The contents of the "Referer: " header to be used in a HTTP request.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setReferer($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_REFERER] = $string;
        } else {
            throw new \Exception('CURLOPT_REFERER is not a string');
        }
        return $this;
    }

    /**
     * A string containing 32 hexadecimal digits. The string should be the MD5 checksum of the remote host's public key, and libcurl will reject the connection to the host unless the md5sums match. This option is only for SCP and SFTP transfers.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setSshHostPublicKeyMd5($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_SSH_HOST_PUBLIC_KEY_MD5] = $string;
        } else {
            throw new \Exception('CURLOPT_SSH_HOST_PUBLIC_KEY_MD5 is not a string');
        }
        return $this;
    }

    /**
     * The file name for your public key. If not used, libcurl defaults to $HOME/.ssh/id_dsa.pub if the HOME environment variable is set, and just "id_dsa.pub" in the current directory if HOME is not set.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setSshPublicKeyfile($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_SSH_PUBLIC_KEYFILE] = $string;
        } else {
            throw new \Exception('CURLOPT_SSH_PUBLIC_KEYFILE is not a string');
        }
        return $this;
    }

    /**
     * The file name for your private key. If not used, libcurl defaults to $HOME/.ssh/id_dsa if the HOME environment variable is set, and just "id_dsa" in the current directory if HOME is not set. If the file is password-protected, set the password with CURLOPT_KEYPASSWD.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setSshPrivateKeyfile($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_SSH_PRIVATE_KEYFILE] = $string;
        } else {
            throw new \Exception('CURLOPT_SSH_PRIVATE_KEYFILE is not a string');
        }
        return $this;
    }

    /**
     * A list of ciphers to use for SSL. For example, RC4-SHA and TLSv1 are valid cipher lists.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setSslCipherList($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_SSL_CIPHER_LIST] = $string;
        } else {
            throw new \Exception('CURLOPT_SSL_CIPHER_LIST is not a string');
        }
        return $this;
    }

    /**
     * The name of a file containing a PEM formatted certificate.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setSslcert($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_SSLCERT] = $string;
        } else {
            throw new \Exception('CURLOPT_SSLCERT is not a string');
        }
        return $this;
    }

    /**
     * The password required to use the CURLOPT_SSLCERT certificate.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setSslcertpasswd($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_SSLCERTPASSWD] = $string;
        } else {
            throw new \Exception('CURLOPT_SSLCERTPASSWD is not a string');
        }
        return $this;
    }

    /**
     * The format of the certificate. Supported formats are "PEM" (default), "DER", and "ENG".
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setSslcerttype($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_SSLCERTTYPE] = $string;
        } else {
            throw new \Exception('CURLOPT_SSLCERTTYPE is not a string');
        }
        return $this;
    }

    /**
     * The identifier for the crypto engine of the private SSL key specified in CURLOPT_SSLKEY.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setSslengine($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_SSLENGINE] = $string;
        } else {
            throw new \Exception('CURLOPT_SSLENGINE is not a string');
        }
        return $this;
    }

    /**
     * The identifier for the crypto engine used for asymmetric crypto operations.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setSslengineDefault($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_SSLENGINE_DEFAULT] = $string;
        } else {
            throw new \Exception('CURLOPT_SSLENGINE_DEFAULT is not a string');
        }
        return $this;
    }

    /**
     * The name of a file containing a private SSL key.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setSslkey($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_SSLKEY] = $string;
        } else {
            throw new \Exception('CURLOPT_SSLKEY is not a string');
        }
        return $this;
    }

    /**
     * The secret password needed to use the private SSL key specified in CURLOPT_SSLKEY.
     * Note:
     * Since this option contains a sensitive password, remember to keep the PHP script it is contained within safe.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setSslkeypasswd($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_SSLKEYPASSWD] = $string;
        } else {
            throw new \Exception('CURLOPT_SSLKEYPASSWD is not a string');
        }
        return $this;
    }

    /**
     * The key type of the private SSL key specified in CURLOPT_SSLKEY. Supported key types are "PEM" (default), "DER", and "ENG".
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setSslkeytype($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_SSLKEYTYPE] = $string;
        } else {
            throw new \Exception('CURLOPT_SSLKEYTYPE is not a string');
        }
        return $this;
    }

    /**
     * The URL to fetch. This can also be set when initializing a session with curl_init().
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setUrl($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_URL] = $string;
        } else {
            throw new \Exception('CURLOPT_URL is not a string');
        }
        return $this;
    }

    /**
     * The contents of the "User-Agent: " header to be used in a HTTP request.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setUseragent($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_USERAGENT] = $string;
        } else {
            throw new \Exception('CURLOPT_USERAGENT is not a string');
        }
        return $this;
    }

    /**
     * A username and password formatted as "[username]:[password]" to use for the connection.
     *
     * @param string $string
     * @return self
     * @throws \Exception
     */
    public function setUserpwd($string)
    {
        if (is_string($string)) {
            $this->defaultOpt[CURLOPT_USERPWD] = $string;
        } else {
            throw new \Exception('CURLOPT_USERPWD is not a string');
        }
        return $this;
    }

    /* $array is an array for the following values of the parameter: */

    /**
     * An array of HTTP 200 responses that will be treated as valid responses and not as errors.
     *
     * @param array $array
     * @return self
     * @throws \Exception
     */
    public function setHttp200aliases($array)
    {
        if (is_array($array)) {
            $this->defaultOpt[CURLOPT_HTTP200ALIASES] = $array;
        } else {
            throw new \Exception('CURLOPT_HTTP200ALIASES is not an array');
        }
        return $this;
    }

    /**
     * An array of HTTP header fields to set, in the format array('Content-type: text/plain', 'Content-length: 100')
     *
     * @param array $array
     * @return self
     * @throws \Exception
     */
    public function setHttpheader($array)
    {
        if (is_array($array)) {
            $this->defaultOpt[CURLOPT_HTTPHEADER] = $array;
        } else {
            throw new \Exception('CURLOPT_HTTPHEADER is not an array');
        }
        return $this;
    }

    /**
     *  An array of FTP commands to execute on the server after the FTP request has been performed.
     *
     * @param array $array
     * @return self
     * @throws \Exception
     */
    public function setPostquote($array)
    {
        if (is_array($array)) {
            $this->defaultOpt[CURLOPT_POSTQUOTE] = $array;
        } else {
            throw new \Exception('CURLOPT_POSTQUOTE is not an array');
        }
        return $this;
    }

    /**
     * An array of FTP commands to execute on the server prior to the FTP request.
     *
     * @param array $array
     * @return self
     * @throws \Exception
     */
    public function setQuote($array)
    {
        if (is_array($array)) {
            $this->defaultOpt[CURLOPT_QUOTE] = $array;
        } else {
            throw new \Exception('CURLOPT_QUOTE is not an array');
        }
        return $this;
    }

    /* $resource is a stream resource for the following values of the parameter: */

    /**
     * The file that the transfer should be written to. The default is STDOUT (the browser window).
     *
     * @param resource $resource
     * @return self
     * @throws \Exception
     */
    public function setFile($resource)
    {
        if (is_resource($resource)) {
            $this->defaultOpt[CURLOPT_FILE] = $resource;
        } else {
            throw new \Exception('CURLOPT_FILE is not a resource');
        }
        return $this;
    }

    /**
     * The file that the transfer should be read from when uploading.
     *
     * @param resource $resource
     * @return self
     * @throws \Exception
     */
    public function setInfile($resource)
    {
        if (is_resource($resource)) {
            $this->defaultOpt[CURLOPT_INFILE] = $resource;
        } else {
            throw new \Exception('CURLOPT_INFILE is not a resource');
        }
        return $this;
    }

    /**
     * An alternative location to output errors to instead of STDERR.
     *
     * @param resource $resource
     * @return self
     * @throws \Exception
     */
    public function setStderr($resource)
    {
        if (is_resource($resource)) {
            $this->defaultOpt[CURLOPT_STDERR] = $resource;
        } else {
            throw new \Exception('CURLOPT_STDERR is not a resource');
        }
        return $this;
    }

    /**
     * The file that the header part of the transfer is written to.
     *
     * @param resource $resource
     * @return self
     * @throws \Exception
     */
    public function setWriteheader($resource)
    {
        if (is_resource($resource)) {
            $this->defaultOpt[CURLOPT_WRITEHEADER] = $resource;
        } else {
            throw new \Exception('CURLOPT_WRITEHEADER is not a resource');
        }
        return $this;
    }

    /* $resource is a string that is the name of a valid callback function for the following values of the parameter: */

    /**
     * A callback accepting two parameters. The first is the cURL resource, the second is a string with the header data to be written. The header data must be written by this callback. Return the number of bytes written.
     *
     * @param type $fname
     * @return self
     * @throws \Exception
     */
    public function setHeaderfunction($fname)
    {
        if (is_string($fname)) {
            $this->defaultOpt[CURLOPT_HEADERFUNCTION] = $fname;
        } else {
            throw new \Exception('CURLOPT_HEADERFUNCTION is not a string');
        }
        return $this;
    }

    # !!! this constant is not defined !!!#
    /**
     * A callback accepting three parameters. The first is the cURL resource, the second is a string containing a password prompt, and the third is the maximum password length. Return the string containing the password.
     *
     * @param type $fname
     * @return self
     * @throws \Exception
     */
//    public function setPasswdfunction($fname)
//    {
//        if (is_string($fname)) {
//            $this->defaultOpt[CURLOPT_PASSWDFUNCTION] = $fname;
//        } else {
//            throw new \Exception('CURLOPT_PASSWDFUNCTION is not a string');
//        } return $this;
//    }

    /**
     * A callback accepting five parameters. The first is the cURL resource, the second is the total number of bytes expected to be downloaded in this transfer, the third is the number of bytes downloaded so far, the fourth is the total number of bytes expected to be uploaded in this transfer, and the fifth is the number of bytes uploaded so far.
     * Note:
     * he callback is only called when the CURLOPT_NOPROGRESS option is set to FALSE.
     *
     * Return a non-zero value to abort the transfer. In which case, the transfer will set a CURLE_ABORTED_BY_CALLBACK error.
     *
     * @param type $fname
     * @return self
     * @throws \Exception
     */
    public function setProgressfunction($fname)
    {
        if (is_string($fname)) {
            $this->defaultOpt[CURLOPT_PROGRESSFUNCTION] = $fname;
        } else {
            throw new \Exception('CURLOPT_PROGRESSFUNCTION is not a string');
        }
        return $this;
    }

    /**
     * A callback accepting three parameters. The first is the cURL resource, the second is a stream resource provided to cURL through the option CURLOPT_INFILE, and the third is the maximum amount of data to be read. The callback must return a string with a length equal or smaller than the amount of data requested, typically by reading it from the passed stream resource. It should return an empty string to signal EOF.
     *
     * @param type $fname
     * @return self
     * @throws \Exception
     */
    public function setReadfunction($fname)
    {
        if (is_string($fname)) {
            $this->defaultOpt[CURLOPT_READFUNCTION] = $fname;
        } else {
            throw new \Exception('CURLOPT_READFUNCTION is not a string');
        }
        return $this;
    }

    /**
     * A callback accepting two parameters. The first is the cURL resource, and the second is a string with the data to be written. The data must be saved by this callback. It must return the exact number of bytes written or the transfer will be aborted with an error.
     *
     * @param type $fname
     * @return self
     * @throws \Exception
     */
    public function setWritefunction($fname)
    {
        if (is_string($fname)) {
            $this->defaultOpt[CURLOPT_WRITEFUNCTION] = $fname;
        } else {
            throw new \Exception('CURLOPT_WRITEFUNCTION is not a string');
        }
        return $this;
    }

    /*     * ************************* */

    /**
     * Get the configuration send to curl_set_opt();
     *
     * @return array
     */
    public function getDefaultOpt()
    {
        $config = $this->defaultOpt;
        foreach ($config as $param => $val) {
            if ($param == CURLOPT_HEADERFUNCTION) {
                $val = $val[1];
            }
            $formated_config[$this->transConst[$param]] = $val;
        }
        return $formated_config;
    }
}

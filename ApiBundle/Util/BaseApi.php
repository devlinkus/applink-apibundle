<?php

namespace App\AppLink\ApiBundle\Util;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use App\AppLink\ApiBundle\Service\App\BreakService as ApiBreak;
use App\AppLink\ApiBundle\Service\App\ApiParameterManager as ApiParam;
use App\AppLink\ApiBundle\Service\App\ApiLogManager as ApiLog;
use App\AppLink\ApiBundle\Lib\Curl\Curl;

/**
 * Abstract class, containt all services and confiiguration
 * required
 *
 * @author linkus
 */
abstract class BaseApi
{
    /**
     * @var \Symfony\Component\HttpFoundation\RequestStack
     */
    protected $requestStack;

    /**
     * @var array
     */
    protected $request;
    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     */
    protected $session;

    /**
     * @var \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected $redirectBase;

    /**
     * @var \AppLink\ApiBundle\Service\App\ApiLogManager|null
     */
    protected $log = null;

    /**
     * @var \AppLink\ApiBundle\Service\App\BreakService
     */
    protected $break = null;

    /**
     * @var \AppLink\ApiBundle\Service\App\ApiParameterManager
     */
    protected $param = null;

    /**
     * @var array configuration of a curl request type|null
     */
    protected $curlCfg;

    /**
     * @var \AppLink\ApiBundle\Lib\Curl\Curl
     */
    protected $curl;

    /**
     * @var string path to cache directory
     */
    protected $cacheDir;


    /**
     * Initialisation of services
     *
     * @param RequestStack $requestStack
     * @param Session $session
     */
    public function initMain(RequestStack $requestStack, Session $session, $cacheDir)
    {
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getMasterRequest();
        $this->session = $session;
        $this->curl = new Curl();
        $this->cacheDir = $cacheDir;
    }

    /**
     * Initialisation of service api.app.log (optional)
     *
     * @param ApiLog $log
     */
    public function initLog(ApiLog $log = null)
    {
        $this->log = $log;
    }

    /**
     * Initialisation of service api.app.break (optional)
     * @param ApiBreak $break
     */
    public function initBreak(ApiBreak $break = null)
    {
        $this->break = $break;
    }

    /**
     * Initialisation of service api.app.break (optional)
     * @param ApiBreak $break
     */
    public function initParam(ApiParam $param = null)
    {
        $this->param = $param;
    }

    /**
     * Load configurations type for curl
     *
     * @param array $curlCfg
     */
    public function initCurlCgf(array $curlCfg)
    {
        $this->curlCfg = $curlCfg;
    }

    /**
     * Set a redirect response
     * set $uri to null to get referer
     *
     * @param mixed $uri
     *
     * @return void
     *
     */
    protected function setRedirect($uri = null)
    {
        if (is_null($uri)) {
            $redirect = $this->requestStack->getMasterRequest();
            $uri = $redirect->headers->get('referer');
        }
        $this->redirectBase = new RedirectResponse($uri);
    }

    /**
     * Return a redirect response to the uri referer
     *
     * @return RedirectResponse
     */
    public function getRedirect()
    {
        return $this->redirectBase;
    }

    /**
     * Send a 'flash' message by session
     *
     * @param string $type
     * @param string $message
     *
     * @return void
     */
    protected function flashMessage($type, $message)
    {
        $this->session->getFlashBag()->add($type, $message);
    }

    /**
     * Set a configuration for curl
     *
     * @param string $set
     *
     * @throws Exception
     */
    protected function setCurlConfig($set = null)
    {
        $cfg = $this->curlCfg;
        if (is_null($set)) {
            echo '<h3>cURL config avaible</h3><pre>';
            var_export($cfg);
            echo '</pre>';
            exit;
        }
        if (!isset($cfg[$set])) {
            throw new \Exception(__METHOD__.': config "' . $set . '" for cURL not found');
        }
        $this->curl->setConfig($cfg[$set]);
    }
}

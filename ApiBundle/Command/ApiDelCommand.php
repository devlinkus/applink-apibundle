<?php

namespace App\AppLink\ApiBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Delete a tool with command: <kbd>linkus:api:del</kbd>
 *
 * This command delete all class, configuration, html test files and template
 * files related to the tool selected.
 *
 * @author linkus
 */
class ApiDelCommand extends Command
{
    protected static $defaultName = 'applink:api:del';

    protected $twig;

    protected function configure()
    {
        $this
            ->setDescription('Delete an application');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $root = realpath(__DIR__ . '/..');
        $apps = array_diff(scandir($root . '/Tool'), ['.', '..', 'ToolDOM.php', 'BaseTool.php']);
        $tools = [];
        foreach ($apps as $t) {
            $tools[] = str_replace('.php', '', $t);
        }
        $io = new SymfonyStyle($input, $output);
        $io->title('Api: Del application');
        if (empty($tools)) {
            $io->block('nothing to delete');
            return;
        }

        $tool = $io->choice('Select the app to delete', $tools);
        $ltool = strtolower($tool);

        $path_tool = $root . '/Tool/' . $tool . '.php';                         //tool
        $path_controller = $root . '/Controller/' . $tool . 'Controller.php';   //controller
        $path_util = $root . '/Util/' . $tool;                                  //util
        $path_service_cfg = $root . '/Resources/config/api_' . $ltool . '.yaml'; //service
//        $path_di = $root . '/DependencyInjection/AppLinkApiExtension.php';      //dependency injection update
        $path_view = $root . '/Resources/views/' . $tool;                       //view
        $path_test = $root . '/HTMLtest/' . $tool;                              //html test files
        // delete related file in Tool/
        if (true === file_exists($path_tool)) {
            unlink($path_tool);
        }

        // delete related folder in Util/
        if (true === file_exists($path_util)) {
            $f = array_diff(scandir($path_util), ['.', '..']);
            foreach ($f as $file) {
                unlink($path_util . '/' . $file);
            }
            rmdir($path_util);
        }

        // delete controller tool
        if (true === file_exists($path_controller)) {
            unlink($path_controller);
        }

        // delete service configuration file
        if (true === file_exists($path_service_cfg)) {
            unlink($path_service_cfg);
        }


        // update dependency injection
//        $extphp = file_get_contents($path_di);
//        $loadertool = '$loader->load(\'api_' . $ltool . '.yaml\');';
//        $nextphp = str_replace("        $loadertool\n", '', $extphp);
//        file_put_contents($path_di, $nextphp);

        // delete related folder in Resources/views/
        if (true === file_exists($path_view)) {
            $f = array_diff(scandir($path_view), ['.', '..']);
            foreach ($f as $file) {
                unlink($path_view . '/' . $file);
            }
            rmdir($path_view);
        }

        // delete related folder in HTMLtest/
        if (true === file_exists($path_test)) {
            $f = array_diff(scandir($path_test), ['.', '..']);
            foreach ($f as $file) {
                unlink($path_test . '/' . $file);
            }
            rmdir($path_test);
        }
    }
}

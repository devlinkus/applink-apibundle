<?php

namespace App\AppLink\ApiBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Export a generated tool with the command <kbd>linkus:api:export</kbd>
 *
 * @author linkus
 */
class ApiExportCommand extends Command
{
    protected static $defaultName = 'applink:api:export';

    protected function configure()
    {
        $this
                ->setDescription('Export an application')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $root = realpath(__DIR__ . '/..');

        $io->title('Api: Export all Apis');

        $apps = array_diff(scandir($root . '/Tool'), ['.', '..', 'ToolDOM.php', 'BaseTool.php']);
        foreach ($apps as $t) {
            $tools[] = str_replace('.php', '', $t);
        }
        if (empty($tools)) {
            $io->block('nothing to export');
            return;
        } else {
            $io->block('api avaible:');
            $io->listing($tools);
        }
        $exPath = $io->ask('Path to export', null, function ($exPath) {
            if (false === is_writable(realpath($exPath))) {
                throw new \RuntimeException('path is not writable');
            }
            if (false == is_dir($exPath)) {
                throw new \RuntimeException('path is not a directory');
            }
            if (!empty(array_diff(scandir($exPath), ['.', '..']))) {
                throw new \RuntimeException('path is not empty');
            }
            return realpath($exPath);
        });

        #load Files
        $files = [];

        $objects = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($root), \RecursiveIteratorIterator::SELF_FIRST);
        foreach ($objects as $name => $object) {
            $bname = basename($name);
            $rp = str_replace($root, '', $name);
            if (in_array($bname, ['.', '..', 'Configuration.php', 'AppLinkApiBundle.php', 'services.yaml',
                        'BaseTool.php', 'ToolDOM.php'
                    ])) {
                continue;
            }
            $masksFile = [
                'services_[bp]', 'ApiAppParameter', 'ApiAppLog', '~$'
            ];
            foreach ($masksFile as $mask) {
                if (preg_match('#' . $mask . '#', $bname)) {
                    continue 2;
                }
            }
            $masksPath = [
                '^/Command', '[er]/App|ws/_App', '^/Lib', '/public', 's/doc', 'views/layout'
            ];
            foreach ($masksPath as $mask) {
                if (preg_match('#' . $mask . '#', $rp)) {
                    continue 2;
                }
            }
            $files[] = $rp;
        }



        foreach ($files as $file) {
            $c = $root.$file;
            $n = $exPath.$file;
            if (is_file($c)) {
                $io->comment('copy file '.$file);
                copy($c, $n);
            }
            if (is_dir($c)) {
                $io->comment('create directory '.$file);
                mkdir($n);
            }
        }

        $servicesLoaded = $this->getContainer()->get('api.services_loaded')->getList();
        if (in_array('api.app.parameter', $servicesLoaded)) {
            $data = $this->getContainer()->get('api.app.parameter')->getParams();
            if (!empty($data)) {
                $json = json_encode($data);
                file_put_contents($exPath.'/parameters.json', $json);
                $io->comment('export parameters [ok]');
            }
        }
    }
}

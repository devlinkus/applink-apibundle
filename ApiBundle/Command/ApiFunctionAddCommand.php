<?php

namespace App\AppLink\ApiBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Add a new service on a tool with command <kbd>linkus:api:fct</kbd>
 *
 * @author linkus
 */
class ApiFunctionAddCommand extends Command
{
    protected static $defaultName = 'applink:api:fct';

    protected $twig;

    protected function configure()
    {
        $this
            ->setDescription('Add A class to an api app');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $root = realpath(__DIR__ . '/..');
        $io = new SymfonyStyle($input, $output);
        $io->title('Api: ADD new function');

        $pName = $io->ask('ApplicationName/ClassName');

        $appName = explode('/', $pName)[0];
        $fctName = explode('/', $pName)[1];

        $pAppName = ($root . '/Util/' . $appName);
        $pFct = $pAppName . '/' . $fctName;

        if (!is_dir($pAppName)) {
            $io->error('Application ' . $appName . ' don\'t exist');
            exit;
        }
        if (!preg_match('#^[A-Z0-9]{1}[A-Za-z0-9]+$#', $fctName)) {
            $io->error('Invalid className.');
            exit;
        }
        if (true === file_exists($pFct . '.php')) {
            $io->error('Class ' . $fctName . ' already exist');
            exit;
        }

        $twig = $this->getTwigEnvironment(__DIR__ . '/skeleton/');

        $path_service_cfg = $root . '/Resources/config/api_' . strtolower($appName) . '.yaml';
        $path_class = $pFct . '.php';

        #class
        $classApp = $twig->render('loadList.php.twig', [
            'name' => lcfirst($appName),
            'className' => $fctName,
            'fctName' => lcfirst($fctName),
        ]);
        file_put_contents($path_class, $classApp);
        $io->section('creation of: ' . $path_class);

        #service update
        $service = file_get_contents($path_service_cfg);
        $addService = $twig->render('service_fct.yaml.twig', [
            'name' => $appName,
            'className' => $fctName,
            'service_name' => lcfirst($fctName),
        ]);
        $nservice = $service . $addService;
        file_put_contents($path_service_cfg, $nservice);
        $io->section('update of: ' . $path_service_cfg);
    }

    protected function getTwigEnvironment($skeletonDir)
    {
        return new \Twig_Environment(new \Twig_Loader_Filesystem($skeletonDir), array(
            'debug' => true,
            'cache' => false,
            'strict_variables' => true,
            'autoescape' => false,
        ));
    }
}

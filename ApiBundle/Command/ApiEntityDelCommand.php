<?php

namespace App\AppLink\ApiBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Delete an entity with command: <kbd>linkus:api:entity:del</kbd>
 *
 * @author linkus
 */
class ApiEntityDelCommand extends Command
{
    protected static $defaultName = 'applink:api:entity:del';

    protected $twig;

    protected function configure()
    {
        $this
                ->setDescription('Delete an entity')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $root = realpath(__DIR__ . '/..');
        $io = new SymfonyStyle($input, $output);

        $rootEntity = $root . '/Entity';

        $io->title('Api: DELETE an entity');

        $listRaw = array_diff(scandir($rootEntity), ['.', '..', 'ApiAppLog.php', 'ApiAppLog.php~', 'ApiAppParameter.php']);
        if (empty($listRaw)) {
            $io->note('nothing to delete');
            exit;
        }
        $list = [];
        foreach ($listRaw as $l) {
            if (preg_match('#Repository\.php$|~#', $l)) {
                continue;
            }
            $list[] = preg_replace('#\.php$#', '', $l);
        }

        $entityName = $io->choice('Select an entity to delete', $list);


        $twig = $this->getTwigEnvironment(__DIR__ . '/skeleton/');

        $service_manager_path = $root . '/Resources/config/manager/'.$entityName.'_manager.yaml';
        $managerDir = $root . '/Manager';
        $path_manager = $managerDir . '/' . $entityName . 'Manager.php';
        $path_entity = $rootEntity . '/' . $entityName . '.php';
        $path_repository = $rootEntity . '/' . $entityName . 'Repository.php';
        $path_controller = $root . '/Controller/' . $entityName . 'Controller.php';
        $viewDir = $root . '/Resources/views/' . $entityName;
        $viewIndex = $viewDir . '/index.html.twig';


        foreach ([$path_entity, $path_controller, $path_repository, $path_manager, $service_manager_path] as $p) {
            if (true === file_exists($p)) {
                unlink($p);
            }
        }

        $filesView = array_diff(scandir($viewDir), ['.', '..']);
        foreach ($filesView as $f) {
            if (true === file_exists($viewDir.'/'.$f)) {
                unlink($viewDir.'/'.$f);
            }
        }

        rmdir($viewDir);
    }

    protected function getTwigEnvironment($skeletonDir)
    {
        return new \Twig_Environment(new \Twig_Loader_Filesystem($skeletonDir), array(
            'debug' => true,
            'cache' => false,
            'strict_variables' => true,
            'autoescape' => false,
        ));
    }
}

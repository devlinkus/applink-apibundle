<?php

namespace App\AppLink\ApiBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Generate new tool with command: <kbd>linkus:api:add</kbd>
 *
 * This command create all class, configuration and template files.
 *
 * @author linkus
 */
class ApiAddCommand extends Command
{
    protected static $defaultName = 'applink:api:add';

    protected $twig;

    protected function configure()
    {
        $this
            ->setDescription('Generate skeleton application');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $root = realpath(__DIR__ . '/..');
        $io = new SymfonyStyle($input, $output);
        $io->title('Api: ADD application');
        $app_name = $io->ask('Name of the app ?');
        if (!preg_match('#^[A-Z]{1}[A-Za-z0-9]{2,11}$#', $app_name)) {
            throw new \Exception('Name should match: ^[A-Z]{1}[A-Za-z0-9]{2,11}$');
        }
        $stlName = strtolower($app_name);
        $ucName = ucfirst($stlName);

        $twig = $this->getTwigEnvironment(__DIR__ . '/skeleton/');

        $path_tool = $root . '/Tool/' . $ucName . '.php';                                   //tool
        $path_tool_test = $root . '/HTMLtest/' . $ucName;                                   //tool test directory
        $path_class = $root . '/Util/' . $ucName . '/LoadList.php';                         //class
        $path_controller = $root . '/Controller/' . $ucName . 'Controller.php';             //controller
        $path_service_cfg = $root . '/Resources/config/api_' . $stlName . '.yaml';           //service
//        $path_di = $root . '/DependencyInjection/AppLinkApiExtension.php';                  //dependency injection update
        $path_view_index = $root . '/Resources/views/' . $ucName . '/index.html.twig';      //view index
        $path_view_test = $root . '/Resources/views/' . $ucName . '/_test.html.twig';        //view test

        #tool
        $tool = $twig->render('tool.php.twig', ['name' => $app_name]);
        if (true !== file_exists($path_tool)) {
            file_put_contents($path_tool, $tool);
            $io->section('creation of: ' . $path_tool);
        }
        if (true !== file_exists($path_tool_test)) {
            mkdir($path_tool_test);
            $io->section('creation of: ' . $path_tool_test);
        }


        #class
        $classApp = $twig->render('loadList.php.twig', ['name' => $app_name]);
        if (true !== file_exists($path_class)) {
            if (true !== file_exists($root . '/Util/' . $ucName)) {
                mkdir($root . '/Util/' . $ucName);
            }
            file_put_contents($path_class, $classApp);
            $io->section('creation of: ' . $path_class);
        }

        #controller
        $controller = $twig->render('controller.php.twig', ['name' => $app_name]);
        if (true !== file_exists($path_controller)) {
            file_put_contents($path_controller, $controller);
            $io->section('creation of: ' . $path_controller);
        }

        #service
        $service = $twig->render('service.yaml.twig', ['name' => $app_name]);
        if (true !== file_exists($path_service_cfg)) {
            file_put_contents($path_service_cfg, $service);
            $io->section('creation of: ' . $path_service_cfg);
        }

        #dependency injection update
//        $extension = $twig->render('extension_line.php.twig', ['name' => $app_name]);
//        $extphp = file_get_contents($path_di);
//        $nextphp = str_replace('//ApiServiceGenerator//', $extension, $extphp);
//        file_put_contents($path_di, $nextphp);

        #view index
        $homepagehtml = $twig->render('index.html.twig.twig', ['name' => $app_name]);
        if (true !== file_exists($path_view_index)) {
            if (true !== file_exists($root . '/Resources/views/' . $ucName)) {
                mkdir($root . '/Resources/views/' . $ucName);
            }
            file_put_contents($path_view_index, $homepagehtml);
            $io->section('creation of: ' . $path_view_index);
        }

        #view test
        $testtool = $twig->render('test_tool.html.twig.twig', []);
        if (true !== file_exists($path_view_test)) {
            file_put_contents($path_view_test, $testtool);
        }
        $io->section('creation of: ' . $path_view_test);
    }

    protected function getTwigEnvironment($skeletonDir)
    {
        return new \Twig_Environment(new \Twig_Loader_Filesystem($skeletonDir), array(
            'debug' => true,
            'cache' => false,
            'strict_variables' => true,
            'autoescape' => false,
        ));
    }
}

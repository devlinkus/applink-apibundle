<?php

namespace App\AppLink\ApiBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Generate a new entity with command: <kbd>linkus:api:entity:add</kbd>
 *
 * @author linkus
 */
class ApiEntityAddCommand extends Command
{
    protected static $defaultName = 'applink:api:entity:add';

    protected $twig;

    protected function configure()
    {
        $this
            ->setDescription('Add an entity');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $root = realpath(__DIR__ . '/..');
        $io = new SymfonyStyle($input, $output);

        $rootEntity = $root . '/Entity';

        $io->title('Api: ADD new entity (without "Api" prefix)');
        $io->note('Be sure to add service entity');

        $entityNameRaw = $io->ask('Entity name');

        $entityName = 'Api' . $entityNameRaw;

        $service_manager_path = $root . '/Resources/config/manager/'.$entityName.'_manager.yaml';
        $managerDir = $root . '/Manager';
        $path_manager = $managerDir . '/' . $entityName . 'Manager.php';
        $path_entity = $rootEntity . '/' . $entityName . '.php';
        $path_repository = $rootEntity . '/' . $entityName . 'Repository.php';
        $path_controller = $root . '/Controller/' . $entityName . 'Controller.php';
        $viewDir = $root . '/Resources/views/' . $entityName;
        $viewIndex = $viewDir . '/index.html.twig';
        $viewShow = $viewDir . '/show.html.twig';

        if (true === file_exists($path_entity)) {
            $io->error('Entity "' . $entityName . '" already exist.');
            exit;
        }
        if (!preg_match('#^Api[A-Z0-9]{1}[A-Za-z0-9]+$#', $entityName)) {
            $io->error('Invalid entity name.');
            exit;
        }
        if (in_array($entityName, ['ApiAppParameter', 'ApiAppLog'])) {
            $io->error('"' . $entityName . '" is a reserverd name (used in servide)');
        }


        $twig = $this->getTwigEnvironment(__DIR__ . '/skeleton/');

        #entity class
        $class_entity = $twig->render('entity.php.twig', ['entityName' => $entityName]);
        if (true !== file_exists($path_entity)) {
            file_put_contents($path_entity, $class_entity);
            $io->section('creation of: ' . $path_entity);
        }

        #repository class
        $class_repository = $twig->render('repository.php.twig', ['entityName' => $entityName]);
        if (true !== file_exists($path_repository)) {
            file_put_contents($path_repository, $class_repository);
            $io->section('creation of: ' . $path_repository);
        }

        #manager
        $class_manager = $twig->render('manager.php.twig', ['entityName' => $entityName]);
        if (true !== file_exists($managerDir)) {
            mkdir($managerDir);
        }
        if (true !== file_exists($path_manager)) {
            file_put_contents($path_manager, $class_manager);
            $io->section('creation of: ' . $path_manager);
        }

        #controller
        $class_controller = $twig->render('entity_controller.php.twig', ['entityName' => $entityName]);
        if (true !== file_exists($path_controller)) {
            file_put_contents($path_controller, $class_controller);
            $io->section('creation of: ' . $path_controller);
        }

        #view index
        $view_index = $twig->render('entity_index.html.twig.twig', ['entityName' => $entityName]);
        if (true !== file_exists($viewDir)) {
            mkdir($viewDir);
        }
        if (true !== file_exists($viewIndex)) {
            file_put_contents($viewIndex, $view_index);
            $io->section('creation of: ' . $viewIndex);
        }
        #view show
        $view_show = $twig->render('entity_show.html.twig.twig', ['entityName' => $entityName]);
        if (true !== file_exists($viewShow)) {
            file_put_contents($viewShow, $view_show);
            $io->section('creation of: ' . $viewShow);
        }

        #add service manager
        $service_manager = $twig->render('service_manager.yaml.twig', [
            'entityName' => $entityName,
        ]);
        file_put_contents($service_manager_path, $service_manager);



        #launch doctrine command (update database)
        $launch = $io->choice('Update database?', ['yes', 'no'], 'yes');
        if ($launch == 'yes') {
            $commandUpdateEntity = $this->getApplication()->find('doctrine:schema:update');
            $arguments = array(
                'command' => 'doctrine:schema:update',
                '--force' => true
            );
            $comInput = new \Symfony\Component\Console\Input\ArrayInput($arguments);
            $commandUpdateEntity->run($comInput, $output);
        }
    }

    protected function getTwigEnvironment($skeletonDir)
    {
        return new \Twig_Environment(new \Twig_Loader_Filesystem($skeletonDir), array(
            'debug' => true,
            'cache' => false,
            'strict_variables' => true,
            'autoescape' => false,
        ));
    }
}

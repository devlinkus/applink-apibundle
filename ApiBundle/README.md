
Installation
============


Copy bundle to

```src/AppLink/```


bundle config:

create `config/packages/app_link_api.yaml` and add:

```yaml
app_link_api:
    app:
        break: ~
        log: ~
        entity: ~
        parameter: ~
```

in `config/bundles.php` add:

```php
return [
    // ...
    App\AppLink\ApiBundle\AppLinkApiBundle::class => ['all' => true],
];
```

in `config/routes.yaml` add:
```yaml
api_bundle_app:
    resource: '../src/AppLink/ApiBundle/Controller/App'
    type:     annotation
    prefix:   /api_scrapper

api_bundle:
    resource: '../src/AppLink/ApiBundle/Controller'
    type:     annotation
    prefix:   /api_scrapper
```

in `config/services.yaml` update:

```yaml
services:
    # ...
    App\:
        resource: '../src/*'
        exclude: '../src/{DependencyInjection,Entity,Migrations,Tests,AppLink,Kernel.php}'
```
